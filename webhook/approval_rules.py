"""Library to represent MR Approval Rules."""
from dataclasses import dataclass
from dataclasses import field
from enum import IntEnum
from enum import auto
from functools import cached_property

from cki_lib.logger import get_logger
from cki_lib.misc import get_nested_key

from webhook.users import User

LOGGER = get_logger('cki.webhook.approval_rules')


class ApprovalRuleType(IntEnum):
    """Possible Types for an Approval Rule."""

    # https://docs.gitlab.com/ee/api/graphql/reference/#approvalruletype
    UNKNOWN = auto()
    ANY_APPROVER = auto()
    CODE_OWNER = auto()
    REGULAR = auto()
    REPORT_APPROVER = auto()
    # custom types
    BLOCKING = auto()        # a rule created from the /block action
    OWNERS = auto()          # a rule generated from an owners.Entry
    SOURCE = auto()          # a Source rule, predefined on the project or group

    @classmethod
    def get(cls, type_str):
        """Return the ApprovalType matching the type string, or Unknown."""
        return next((member for name, member in cls.__members__.items()
                     if name == type_str.upper()), cls.UNKNOWN)


@dataclass(frozen=True)
class ApprovalUser:
    """Wrap a User with approval status."""

    user: User
    approved: bool = field(default=False, compare=False)

    @classmethod
    def new(cls, user_cache, user_dict, approved):
        """Create a User from the user_dict using the user_cache."""
        return cls(user_cache.get_by_dict(user_dict), approved)

    def __getattr__(self, value):
        """Return the attribute from the User so we can sort of pretend we are one. Ha."""
        return getattr(self.user, value)


class ApprovalRule:
    """An Approval Rule."""

    def __init__(self, user_cache, input_dict, source_rule=False):
        """Show yourself."""
        self.user_cache = user_cache
        self.data = input_dict
        self.source_rule = source_rule
        LOGGER.debug('Created %s', self)

    def __eq__(self, other):
        """Return True if the name, required, and eligible match, otherwise False."""
        return (self.name, self.required, self.eligible) == \
            (other.name, other.required, other.eligible)

    def __repr__(self):
        """Return a string representing the Rule."""
        if ApprovalRuleType.get(self.data.get('type', '')) is ApprovalRuleType.ANY_APPROVER:
            eligible = '∞'
        else:
            eligible = len(self.eligible)
        rule_str = f"Rule '{self.name}' ({self.type.name}):"
        rule_str += f' eligible: {eligible}, required: {self.required}'
        if not self.source_rule:
            rule_str += f', approved by: {len(self.approved_by)}'
            if self.required:
                rule_str += f', approved: {self.approved}'
        return f'<{rule_str}>'

    @property
    def approved(self):
        """Return True if self.approved_by >= self.required."""
        return len(self.approved_by) >= self.required

    @cached_property
    def approved_by(self):
        """Return the set of Users that have given their approval."""
        approved_by = get_nested_key(self.data, 'approvedBy/nodes', [])
        return {ApprovalUser.new(self.user_cache, user, True) for user in approved_by}

    @cached_property
    def eligible(self):
        """Return the set of eligible ApprovalUsers."""
        eligible = get_nested_key(self.data, 'eligibleApprovers', [])
        approved_by = get_nested_key(self.data, 'approvedBy/nodes', [])
        return {ApprovalUser.new(self.user_cache, user, user in approved_by) for user in eligible}

    @property
    def id(self):
        # pylint: disable=invalid-name
        """Return the rule ID as an int."""
        return int(self.data['id'].rsplit('/', 1)[-1]) if 'id' in self.data else 0

    @cached_property
    def missing(self):
        """Return the set of Users that could not be found on the project."""
        # Only for a rule that was generated from an owners Entry... right?
        missing_list = self.data.get('missing', [])
        return {self.user_cache.get_by_dict(user) for user in missing_list}

    @property
    def name(self):
        """Return the rule name."""
        return self.data.get('name', '')

    @property
    def overridden(self):
        """Return whether the rule has been overridden."""
        return self.data.get('overridden', False)

    @property
    def required(self):
        """Return the number of approvals required."""
        return self.data.get('approvalsRequired', 0)

    @cached_property
    def source(self):
        """Return the source ApprovalRule, if any."""
        if not (raw_source := self.data.get('sourceRule')):
            return None
        # A source rule does not provide eligible users so copy them from the parent.
        raw_source['eligibleApprovers'] = get_nested_key(self.data, 'eligibleApprovers', [])
        raw_source['approvedBy'] = get_nested_key(self.data, 'approvedBy', {'nodes': []})
        return ApprovalRule(self.user_cache, raw_source, source_rule=True)

    @property
    def type(self):
        """Return the rule type."""
        return ApprovalRuleType.SOURCE if self.source_rule else \
            ApprovalRuleType.get(self.data.get('type', ''))

    @classmethod
    def from_entry(cls, user_cache, entry, author=None):
        """Construct an ApprovalRule from an owners.Entry."""
        approvals_required = 1 if entry.required_approvals else 0
        approvers = []
        missing_approvers = []
        # The eligible approvers for a given subsystem are the set of all the listed reviewers and
        # maintainers but we exclude restricted users if the subsystem requires approvals.
        for approver in entry.reviewers + entry.maintainers:
            if approvals_required and approver.get('restricted'):
                LOGGER.info('Excluding %s from %s rule as they are restricted.',
                            approver['gluser'], entry.subsystem_label)
                continue
            # Exclude the given author from the list of approvers.
            if author and author.username == approver['gluser']:
                LOGGER.info('Excluding %s from %s rule as they are the author.',
                            approver['gluser'], entry.subsystem_label)
                continue
            # It is possible an owners entry username isn't a project member which means we won't be
            # able to make them part of an approval rule. Make sure to separate those users out.
            if user_cache.get_by_username(approver['gluser']):
                approvers.append(approver)
            else:
                missing_approvers.append(approver)
        entry_dict = {'eligibleApprovers': approvers,
                      'missing': missing_approvers,
                      'name': entry.subsystem_label,
                      'approvalsRequired': approvals_required,
                      'type': ApprovalRuleType.OWNERS.name}
        return cls(user_cache, entry_dict)
