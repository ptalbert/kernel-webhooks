"""Ensure a merge request is mergeable."""
from datetime import datetime
from datetime import timedelta
import os
import subprocess
import sys

from cki_lib import logger
from cki_lib import misc

from . import common
from . import defs

LOGGER = logger.get_logger('cki.webhook.mergehook')


# Get all open MRs.
MR_QUERY = """
query mrData($namespace: ID!, $branches: [String!], $first: Boolean = true, $after: String = "") {
  project(fullPath: $namespace) {
    id @include(if: $first)
    mergeRequests(state: opened, after: $after, targetBranches: $branches) {
      pageInfo {hasNextPage endCursor}
      nodes {iid author{username} title targetBranch project{fullPath} labels{nodes{title}}}
    }
  }
}
"""


def _git(rhkernel_src, args, **kwargs):
    """Run a git via subprocess (for things GitPython doesn't support)."""
    kwargs.setdefault('check', True)
    # pylint: disable=subprocess-run-check
    LOGGER.debug("Running command: git %s", " ".join(args))
    return subprocess.run(['git'] + args, cwd=rhkernel_src,
                          capture_output=True, text=True, **kwargs)


def _git_branch_copy(location, branch):
    """Save a copy of the branch for later use w/git reset."""
    _git(location, ['branch', '--copy', branch])


def _git_reset(location, branch):
    """Reset the current checkout in location to branch."""
    _git(location, ['reset', '--hard', branch])


def _git_merge(location, reference):
    """Merge reference (quietly, with on commit message editing) to location."""
    _git(location, ['merge', '--quiet', '--no-edit', reference])


def _git_branch_delete(location, branch):
    """Delete git branch with extreme prejudice."""
    _git(location, ['branch', '-D', branch])


def _git_worktree_remove(location, worktree):
    """Delete worktree and branch."""
    _git(location, ['worktree', 'remove', '-f', worktree])


def fetch_mr_list(gl_project, gl_mergerequest, graphql):
    """Fetch the list of relevant MRs via graphql query."""
    query_params = {'namespace': gl_project.path_with_namespace,
                    'branches': gl_mergerequest.target_branch}
    result = graphql.check_query_results(
        graphql.client.query(MR_QUERY, query_params, paged_key="project/mergeRequests"),
        check_keys={'project'})
    mr_list = [mr for mr in misc.get_nested_key(result, 'project/mergeRequests/nodes')
               if mr['targetBranch'] == gl_mergerequest.target_branch]

    LOGGER.debug("Returning mr list of %s", mr_list)
    return mr_list


def mr_not_mergeable(worktree_dir, target_branch, merge_branch):
    """Ensure MR can be merged to target branch."""
    _git_reset(worktree_dir, target_branch)
    try:
        _git_merge(worktree_dir, merge_branch)
    except subprocess.CalledProcessError:
        return True

    return False


def build_mr_conflict_string(mr_entry):
    """Build MR conflict report string."""
    mr_id = int(mr_entry['iid'])
    author = mr_entry['author']['username']
    title = mr_entry['title']

    return f"MR !{mr_id} from @{author} (`{title}`) conflicts with this MR.  \n"


def check_for_other_merge_conflicts(gl_project, gl_mergerequest,
                                    graphql, merge_branch, worktree_dir):
    """Check to see if there are any other pending MRs that conflict with this MR."""
    LOGGER.debug("Checking other pending %s MRs for conflicts with MR %d",
                 gl_mergerequest.target_branch, gl_mergerequest.iid)

    mr_list = fetch_mr_list(gl_project, gl_mergerequest, graphql)
    # Save the base mr-is-merged as a branch, so we can quickly reset after each OTHER mr
    save_branch = f"{merge_branch}-save"
    _git_branch_copy(worktree_dir, save_branch)

    conflicts = []
    pname = gl_project.name
    target_branch = f"{pname}/{gl_mergerequest.target_branch}"
    for mr_entry in mr_list:
        mr_id = int(mr_entry['iid'])
        if mr_id == gl_mergerequest.iid:
            continue
        # First, make sure this MR can actually be merged by itself
        if {'title': defs.MERGE_CONFLICT_LABEL} in mr_entry['labels']['nodes']:
            LOGGER.debug("MR %d has conflicts label on it, skipping it", mr_id)
            continue
        if mr_not_mergeable(worktree_dir, target_branch, f'{pname}/merge-requests/{mr_id}'):
            LOGGER.debug("MR %d can't be merged by itself, skipping conflict check", mr_id)
            continue

        # Now, try to merge it on top of this MR, so we can see if it has any conflicts
        _git_reset(worktree_dir, save_branch)
        try:
            _git_merge(worktree_dir, f'{pname}/merge-requests/{mr_id}')
        except subprocess.CalledProcessError as err:
            conflicts.append(build_mr_conflict_string(mr_entry))
            conflicts.append(err.output)

    _git_branch_delete(worktree_dir, save_branch)
    return conflicts


def create_worktree_timestamp(worktree_dir):
    """Create a timestamp file in the worktree."""
    now = datetime.now()
    ts_file = f"{worktree_dir}/timestamp"
    with open(ts_file, 'w', encoding='ascii') as ts_fh:
        ts_fh.write(now.strftime('%Y%m%d%H%M%S'))
        ts_fh.close()


def check_for_merge_conflicts(gl_project, gl_mergerequest, worktree_dir):
    """Check to see if this MR can be cleanly merged to the target branch."""
    mr_id = gl_mergerequest.iid
    target_branch = f"{gl_project.name}/{gl_mergerequest.target_branch}"
    LOGGER.debug("Checking for merge conflicts when merging %d into %s.", mr_id, target_branch)
    conflicts = []
    create_worktree_timestamp(worktree_dir)
    # Now try to merge the MR to the worktree
    try:
        _git_merge(worktree_dir, f'{gl_project.name}/merge-requests/{mr_id}')
    except subprocess.CalledProcessError as err:
        _git_reset(worktree_dir, target_branch)
        conflicts.append(f"MR !{mr_id} cannot be merged to {target_branch}  \n")
        conflicts.append(err.output)

    # if the MR can't be merged to target branch, try to force gitlab's status recheck before
    # we return our git-based conflicts status
    if conflicts and misc.is_production():
        gl_project.mergerequests.list(iids=[mr_id], with_merge_status_recheck=True)

    return conflicts


def update_project_refs(gl_project, rhkernel_src):
    """Make sure we have the latest MR refs for this project locally cached."""
    _git(rhkernel_src, ['fetch', gl_project.name])


def clean_up_temp_merge_branch(rhkernel_src, merge_branch, worktree_dir):
    """Clean up the git worktree and branches from the test merges."""
    _git_worktree_remove(rhkernel_src, worktree_dir)
    _git_branch_delete(rhkernel_src, merge_branch)
    LOGGER.debug("Removed worktree %s and deleted branch %s", worktree_dir, merge_branch)


def handle_stale_worktree(rhkernel_src, merge_branch, worktree_dir):
    """Ensure we're not stomping on another run, and clean it up if it's old."""
    LOGGER.warning("Worktree already exists at %s!", worktree_dir)
    ts_file = f"{worktree_dir}/timestamp"
    if not os.path.exists(ts_file):
        create_worktree_timestamp(worktree_dir)

    with open(ts_file, 'r', encoding='ascii') as ts_fh:
        old_ts = ts_fh.read()
        ts_fh.close()

    if old_ts < (datetime.now() - timedelta(hours=1)).strftime('%Y%m%d%H%M%S'):
        LOGGER.warning("Stale worktree at %s is over 1h old, purging it.", worktree_dir)
        clean_up_temp_merge_branch(rhkernel_src, merge_branch, worktree_dir)
        try:
            _git_branch_delete(worktree_dir, f"{merge_branch}-save")
        except subprocess.CalledProcessError:
            return
    else:
        raise RuntimeError("Existing worktree less than 1h old, trying back later.")


def prep_temp_merge_branch(gl_project, gl_mergerequest, rhkernel_src):
    """Set up the temporary branch/worktree to test mergeability in."""
    # This target_branch is dependent on the repo layout in utils/rh_kernel_git_repos.yml,
    # where everything is an additional remote on top of kernel-ark, w/remote name == project name
    target_branch = f"{gl_project.name}/{gl_mergerequest.target_branch}"
    merge_branch = f"{gl_project.name}-{gl_mergerequest.target_branch}-{gl_mergerequest.iid}"
    worktree_dir = f"/{'/'.join(rhkernel_src.strip('/').split('/')[:-1])}/{merge_branch}-merge/"

    # Ensure we don't have a stale/failed checkout lingering
    if os.path.isdir(worktree_dir):
        handle_stale_worktree(rhkernel_src, merge_branch, worktree_dir)

    # This is the lookaside cache we maintain for examining diffs between revisions of a
    # merge request, which we're going to create temporary worktrees off of
    LOGGER.info("Creating git worktree at %s with branch %s for mergeability testing, please hold",
                worktree_dir, merge_branch)
    _git(rhkernel_src, ['worktree', 'add', '-B', merge_branch, worktree_dir, target_branch])
    return merge_branch, worktree_dir


def format_conflict_info(conflict_info, merge_label):
    """Format the additional merge conflict output for comment output."""
    if merge_label == defs.MERGE_CONFLICT_LABEL:
        comment = "This merge request cannot be merged to its target branch\n\n"
    elif merge_label == defs.MERGE_WARNING_LABEL:
        comment = ('There are other pending MRs that conflict with this one. '
                   'Please discuss possible merge solutions with the author(s) of the other '
                   'merge request(s).\n\n')
    else:
        comment = "This MR can be merged cleanly to its target branch."

    for entry in conflict_info:
        if entry.startswith('MR'):
            comment += f'\n{entry}\n'
        else:
            for line in entry.split('\n'):
                if line.startswith('CONFLICT'):
                    comment += f'* {line}  \n'

    return comment


def process_merge_request(gl_instance, gl_project, gl_mergerequest, graphql, rhkernel_src):
    """Process a merge request."""
    merge_label = f'Merge::{defs.READY_SUFFIX}'
    update_project_refs(gl_project, rhkernel_src)
    merge_branch, worktree_dir = prep_temp_merge_branch(gl_project, gl_mergerequest, rhkernel_src)

    conflict_info = check_for_merge_conflicts(gl_project, gl_mergerequest, worktree_dir)
    if conflict_info:
        merge_label = defs.MERGE_CONFLICT_LABEL
    else:
        conflict_info = check_for_other_merge_conflicts(gl_project, gl_mergerequest,
                                                        graphql, merge_branch, worktree_dir)
        if conflict_info:
            merge_label = defs.MERGE_WARNING_LABEL

    note = f'**Mergeability Summary:** ~"{merge_label}"\n\n'
    note += format_conflict_info(conflict_info, merge_label)

    LOGGER.info("Conflict info:\n%s", note)
    clean_up_temp_merge_branch(rhkernel_src, merge_branch, worktree_dir)
    common.add_label_to_merge_request(gl_project, gl_mergerequest.iid, [merge_label])

    if not common.mr_is_closed(gl_mergerequest):
        common.update_webhook_comment(gl_mergerequest, gl_instance.user.username,
                                      '**Mergeability Summary:', note)


def process_mr_event(gl_instance, msg, graphql, rhkernel_src, **_):
    """Process a merge request event."""
    gl_project = gl_instance.projects.get(msg.payload['project']['path_with_namespace'])
    if not (gl_mergerequest := common.get_mr(gl_project, msg.payload['object_attributes']['iid'])):
        return

    # Only run on MR events where code has changed or the Merge::* label was changed
    label_changed = common.has_label_prefix_changed(msg.payload['changes'], 'Merge::')
    if not common.mr_action_affects_commits(msg) and not label_changed:
        return

    hook_name = "mergehook"
    run_on_drafts = True
    if common.do_not_run_hook(gl_project, gl_mergerequest, hook_name, run_on_drafts):
        return

    process_merge_request(gl_instance, gl_project, gl_mergerequest, graphql, rhkernel_src)


def process_note_event(gl_instance, msg, graphql, rhkernel_src, **_):
    """Process a note message event."""
    if not common.force_webhook_evaluation(msg.payload['object_attributes']['note'],
                                           ['merge', 'mergehook']):
        LOGGER.info('Note event did not request merge evaluation, ignoring.')
        return

    gl_project = gl_instance.projects.get(msg.payload['project']['path_with_namespace'])
    if not (gl_mergerequest := common.get_mr(gl_project, msg.payload['merge_request']['iid'])):
        return

    process_merge_request(gl_instance, gl_project, gl_mergerequest, graphql, rhkernel_src)


WEBHOOKS = {
    'merge_request': process_mr_event,
    'note': process_note_event,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('MERGEHOOK')
    parser.add_argument('--rhkernel-src', **common.get_argparse_environ_opts('RHKERNEL_SRC'),
                        help='Directory containing RH kernel projects git tree')
    args = parser.parse_args(args)
    if not args.rhkernel_src:
        LOGGER.warning("No Linux source tree directory specified, using default")
    common.generic_loop(args, WEBHOOKS, get_graphql_instance=True, rhkernel_src=args.rhkernel_src)


if __name__ == '__main__':
    main(sys.argv[1:])
