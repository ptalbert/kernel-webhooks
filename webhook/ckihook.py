"""Manage the CKI labels."""
from dataclasses import dataclass
import sys
from textwrap import indent

from cki_lib import logger
from cki_lib import misc
from cki_lib.gitlab import get_variables
import prometheus_client as prometheus

from webhook import common
from webhook.base_mr import BaseMR
from webhook.base_mr_mixins import PipelinesMixin
from webhook.pipelines import CKI_LABEL_PREFIXES
from webhook.pipelines import PipelineResult
from webhook.pipelines import PipelineStatus
from webhook.pipelines import PipelineType

LOGGER = logger.get_logger('cki.webhook.ckihook')


METRIC_KWF_CKIHOOK_PIPELINES_RETRIED = prometheus.Counter(
    'kwf_ckihook_pipelines_retried',
    'Number of CKI pipelines retried by ckihook',
    ['project_path']
)

REPORT_HEADER = '**CKI Pipelines Status:**'
INDENT = '    '


@dataclass(repr=False)
class PipeMR(PipelinesMixin, BaseMR):
    """An MR class for pipelines."""


OPEN_MRS_QUERY = """
query mrData($namespace: ID!, $branches: [String!], $first: Boolean = true, $after: String = "") {
  project(fullPath: $namespace) {
    id @include(if: $first)
    mrs: mergeRequests(
      after: $after
      state: opened
      labels: ["CKI_RT::Failed::merge"]
      targetBranches: $branches
    ) {
      pageInfo {
        hasNextPage
        endCursor
      }
      nodes {
        iid
        headPipeline {
          jobs {
            nodes {
              allowFailure
              id
              name
              createdAt
              pipeline {
                id
              }
              status
              downstreamPipeline {
                id
                project {
                  id
                }
                status
                stages {
                  nodes {
                    name
                    jobs {
                      nodes {
                        id
                        name
                        status
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
"""


def cki_label_changed(changes):
    """Return True if any CKI label changed, or False."""
    return any(common.has_label_prefix_changed(changes, f'{prefix}::') for
               prefix in list(CKI_LABEL_PREFIXES.values()) + ['CKI'])


def retry_pipelines(gl_instance, failed_pipelines):
    """Get a gitlab instance and retry the failed pipelines."""
    # Input is a dict with mr_id as key and PipelineResult as value.
    gl_projects = {}
    for mr_id, pipe in failed_pipelines.items():
        prepare_stage = pipe.get_stage('prepare')
        prepare_py_job_id = next((job['id'] for job in prepare_stage['jobs'] if
                                  job['name'] == 'prepare_python'), None) if prepare_stage else None
        if not prepare_py_job_id:
            LOGGER.warning("Downstream pipeline %s does not have a 'prepare python' job to retry?",
                           pipe)
            continue
        gl_project = gl_projects.get(pipe.ds_project_id) or \
            gl_projects.setdefault(pipe.ds_project_id, gl_instance.projects.get(pipe.ds_project_id))
        gl_pipeline = gl_project.pipelines.get(pipe.ds_pipeline_id)
        gl_job = gl_project.jobs.get(int(prepare_py_job_id.rsplit('/', 1)[-1]))

        LOGGER.info('Retrying downstream pipeline #%s (job #%s) on %s for MR %s', gl_pipeline.id,
                    gl_job.id, gl_project.path_with_namespace, mr_id)
        if misc.is_production():
            gl_job.retry()
            gl_pipeline.retry()
            METRIC_KWF_CKIHOOK_PIPELINES_RETRIED.labels(gl_project.path_with_namespace).inc()


def failed_rt_mrs(graphql, namespace, branches):
    """Return a dict of namespace MRs open on the branches that failed CKI_RT merge."""
    query_params = {'namespace': namespace, 'branches': branches}
    result = graphql.client.query(OPEN_MRS_QUERY, query_params, paged_key='project/mrs')
    # The query only returns MRs with a CKI_RT::Failed::merge label so nothing to filter here.
    return {mr['iid']: mr['headPipeline']['jobs']['nodes'] for
            mr in result['project']['mrs']['nodes'][:10]} if result.get('project') else {}


def retrigger_failed_pipelines(graphql, gl_instance, namespace, branch):
    """Retrigger any RT pipelines that failed on or before the MERGE stage."""
    if PipelineType.REALTIME not in branch.pipelines:
        LOGGER.info('This branch does not expect a realtime pipeline: %s', branch.pipelines)
        return
    branch_names = [branch.name, branch.name.removesuffix('-rt')]
    if not (mrs := failed_rt_mrs(graphql, namespace, branch_names)):
        LOGGER.info('No open MRs for %s on branches %s, nothing to do.', namespace, branch_names)
        return
    failed_pipes = {}
    for mr_id, raw_bridge_jobs in mrs.items():
        pipelines = PipelineResult.prepare_pipelines(raw_bridge_jobs)
        if pipe := next((p for p in pipelines if p.label == 'CKI_RT::Failed::merge'), None):
            failed_pipes[mr_id] = pipe

    LOGGER.info('Found %s MRs and %s RT pipelines that failed on Merge for %s.', len(mrs),
                len(failed_pipes), namespace)
    retry_pipelines(gl_instance, failed_pipes)


def get_project_pipeline_var(gl_project, pipeline_id, key):
    """Return the value of the pipeline variable matching key, or None."""
    gl_pipeline = gl_project.pipelines.get(pipeline_id)
    return get_variables(gl_pipeline).get(key)


def get_pipeline_target_branch(gl_instance, project_id, pipeline_id):
    """Return the 'branch' pipeline variable value for the given project/pipeline."""
    gl_project = gl_instance.projects.get(project_id)
    return get_project_pipeline_var(gl_project, pipeline_id, 'branch')


def get_downstream_pipeline_branch(gl_instance, pipe_result):
    """Return the 'branch' var value if found, otherwise None."""
    ds_project_id = pipe_result.ds_project_id
    ds_pipeline_id = pipe_result.ds_pipeline_id

    if not (ds_branch := get_pipeline_target_branch(gl_instance, ds_project_id, ds_pipeline_id)):
        LOGGER.debug("Could not get 'branch' variable from downstream pipeline %s", ds_pipeline_id)
    return ds_branch


def branch_changed(payload):
    """Return True if it seems like the MR branch changed, otherwise False."""
    LOGGER.info('Checking if MR target branch matches latest pipeline target branch.')
    # If the MR doesn't have a head pipeline then we're done here.
    if not payload['object_attributes']['head_pipeline_id']:
        LOGGER.info('No head pipeline, skipping target branch check.')
        return False
    # This is the signature of an MR event when the target branch changes. Maybe?
    if 'merge_status' in payload['changes']:
        return True
    LOGGER.info("'changes' dict does not have 'merge_status' key, skipping target branch check.")
    return False


def process_possible_branch_change(pipe_mr, payload):
    """Confirm MR branch changed and if so, Return True and cancel/trigger pipelines."""
    if not pipe_mr.pipelines:
        LOGGER.info('MR has no pipelines, nothing to do.')
        return False
    ds_branch = get_downstream_pipeline_branch(pipe_mr.gl_instance, pipe_mr.pipelines[0])
    mr_branch = payload['object_attributes']['target_branch']
    if ds_branch and ds_branch != mr_branch:
        LOGGER.info('Target branch changed: MR %s != downstream pipeline %s, triggering...',
                    mr_branch, ds_branch)
        if not misc.is_production():
            LOGGER.info('Not production, skipping work.')
            return True
        head_pipeline_id = pipe_mr.head_pipeline_id
        gl_mr = pipe_mr.gl_mr
        common.cancel_pipeline(pipe_mr.gl_project, head_pipeline_id)
        new_pipeline_id = common.create_mr_pipeline(gl_mr)
        new_pipeline_url = f'{pipe_mr.gl_project.web_url}/-/pipelines/{new_pipeline_id}'
        note_text = ("This MR's current target branch has changed since the last pipeline"
                     f" was triggered. The last pipeline {head_pipeline_id} has been canceled"
                     f" and a new pipeline has been triggered: {new_pipeline_url}  \n"
                     f"Last pipeline MR target branch: {ds_branch}  \n"
                     f"Current MR target branch: {mr_branch}  ")
        pipe_mr.graphql.create_note(pipe_mr.global_id, note_text)
        return True
    LOGGER.info("MR current target branch '%s' matches latest pipeline.", mr_branch)
    return False


def pipeline_is_waived(pipeline, mr_labels):
    """Return True if the pipeline is currently waived, otherwise False."""
    # Only possible to waive RT and Automotive pipelines.
    waiveable = (PipelineType.REALTIME, PipelineType.AUTOMOTIVE)
    # You can't waive a pipeline that isn't failed.
    return pipeline.type in waiveable and f'{pipeline.type.prefix}::Waived' in mr_labels and \
        pipeline.status is PipelineStatus.FAILED


def build_pipeline_report(pipelines, mr_labels):
    """Generate a markdown string showing the pipeline details."""
    def label_name(pipe):
        """Return a markdown formatted representation of the pipeline label and name."""
        pipe_link = f'[{pipe.bridge_name}]({pipe.ds_url})' if pipe.ds_url else pipe.bridge_name
        return f'~"{pipe.label}" {pipe_link}' if pipe.label else pipe_link

    report_str = ''
    failed = [pipe for pipe in pipelines.values() if not pipeline_is_waived(pipe, mr_labels) and
              pipe.status in (PipelineStatus.FAILED, PipelineStatus.CANCELED)]
    running = [pipe for pipe in pipelines.values() if pipe.status is PipelineStatus.RUNNING]
    success = [pipe for pipe in pipelines.values() if pipe.status is PipelineStatus.SUCCESS or
               pipeline_is_waived(pipe, mr_labels)]

    if failed:
        report_str += '##### Failed pipelines\n'
        report_str += 'These pipelines have failed or been canceled.\n'
        for pipe in failed:
            if pipe.status is PipelineStatus.FAILED:
                report_str += f'- {label_name(pipe)} failed at the {pipe.failed_stage.name} stage\n'
                report_str += f'{INDENT}~~~\n{indent(pipe.kcidb_report, INDENT)}\n{INDENT}~~~\n'
            else:
                report_str += f'- {label_name(pipe)} has been canceled\n'
        report_str += '\n'
    if running:
        report_str += '##### Running pipelines\n'
        report_str += 'These pipelines are still running.\n'
        report_str += '\n'.join(f'- {label_name(pipe)}' for pipe in running) + '\n'
    if success:
        report_str += '##### Completed pipelines\n'
        report_str += 'These pipelines have been completed successfully or have been waived.\n'
        for pipe in success:
            report_str += f'- {label_name(pipe)} '
            report_str += 'has been waived\n' if pipeline_is_waived(pipe, mr_labels) else \
                'was successful\n'
    return report_str


def generate_comment(pipe_mr, pipelines, cki_status_label):
    """Generate a markdown string for the given MR & pipelines."""
    comment_str = f'{REPORT_HEADER} ~"{cki_status_label}"\n\n'

    # Report missing pipelines first.
    if missing_ptypes := [ptype for ptype, pipeline in pipelines.items() if pipeline is None]:
        comment_str += '#### Missing pipelines\n\n'
        comment_str += ('The target branch of this MR is expected to include the following pipeline'
                        ' types but they have not been found.  Please contact the webhooks or CKI'
                        ' teams if this problem persists.\n')
        for ptype in missing_ptypes:
            comment_str += f'- {ptype.name}\n'
        comment_str += '\n'

    # Separate the found pipelines into blocking and nonblocking.
    blocking = {ptype: pipe for ptype, pipe in pipelines.items() if pipe and not pipe.allow_failure}
    nonblocking = {ptype: pipe for ptype, pipe in pipelines.items() if pipe and pipe.allow_failure}

    # Report on blocking pipelines.
    if not blocking:
        comment_str += '#### No blocking pipelines found. Perhaps they are missing?\n'
    else:
        comment_str += '#### Blocking pipelines\n'
        comment_str += build_pipeline_report(blocking, pipe_mr.labels)
    # Report on nonblocking pipelines.
    if nonblocking:
        comment_str += '#### Non-blocking pipelines\n'
        comment_str += build_pipeline_report(nonblocking, pipe_mr.labels)
    return comment_str


def generate_pipeline_labels(pipe_mr, pipelines):
    """Return the set of CKI labels derived from the given dict of PipelineResults."""
    cki_labels = set()
    for pipeline_type, pipeline_result in pipelines.items():
        if pipeline_result is None:
            LOGGER.info('Missing result for %s', repr(pipeline_type))
            cki_labels.add(f'{pipeline_type.prefix}::Missing')
            continue
        if not pipeline_result.label:
            LOGGER.info('%s has no label, ignoring result.', pipeline_result)
            continue
        if pipeline_is_waived(pipeline_result, pipe_mr.labels):
            LOGGER.info('MR has Waived label for %s, ignoring result.', pipeline_result)
            continue
        cki_labels.add(pipeline_result.label)
    LOGGER.info('Calculated CKI labels: %s', cki_labels)
    return cki_labels


def generate_status_label(pipe_mr, pipelines):
    """Return a string representing the overall status of all required pipelines."""
    # Get the lowest PipelineStatus of expected results which are not allowed to fail & not waived.
    lowest_status = min((pipe.status for pipe in pipelines.values() if
                         pipe is not None and
                         pipe.type in pipe_mr.branch.pipelines and
                         not pipe.allow_failure and
                         not pipeline_is_waived(pipe, pipe_mr.labels)),
                        default=PipelineStatus.MISSING)
    return f'CKI::{lowest_status.title}'


def update_mr(pipe_mr, pipelines):
    """Update the given MR with a comment and set CKI labels."""
    cki_status_label = generate_status_label(pipe_mr, pipelines)
    all_labels = generate_pipeline_labels(pipe_mr, pipelines) | {cki_status_label}
    # Update the MR labels if needed, making sure to remove double-scoped labels.
    if not all_labels.issubset(set(pipe_mr.labels)):
        pipe_mr.add_labels(all_labels, remove_scoped=True)
    else:
        LOGGER.info('MR already has all the expected labels, nothing to do.')

    # Update the report comment.
    report_text = generate_comment(pipe_mr, pipelines, cki_status_label)
    LOGGER.info('Leaving comment: \n%s', report_text)
    gl_mr = pipe_mr.gl_mr
    common.update_webhook_comment(gl_mr, pipe_mr.current_user.username, REPORT_HEADER, report_text)


def process_pipe_mr(pipe_mr):
    """Populate a sorted dict of the MR's pipelines and call update_mr()."""
    if not pipe_mr.pipelines:
        LOGGER.info('MR has no pipelines, nothing to do.')
        return
    # Create a mapping of PipelineType:PipelineResult pairs.
    pipelines = {}
    for pipeline in pipe_mr.pipelines:
        if pipeline.type in pipelines:
            raise ValueError(f'Multiple pipelines have the same type: {pipeline.type}')
        pipelines[pipeline.type] = pipeline
    # Insert placeholders keys for any missing pipelines.
    for ptype in pipe_mr.branch.pipelines:
        if ptype not in pipelines:
            pipelines[ptype] = None
    # Sort the pipelines so we process them in a consistent order.
    update_mr(pipe_mr, dict(sorted(pipelines.items())))


def get_pipe_mr(graphql, gl_instance, projects, mr_url):
    """Return a new PipeMR object."""
    return PipeMR(graphql, gl_instance, projects, mr_url)


def process_mr_event(gl_instance, msg, projects, graphql, **_):
    """Process an MR message."""
    mr_url = msg.payload['object_attributes']['url']
    LOGGER.info('Processing MR event for %s', mr_url)
    pipe_mr = None
    if branch_changed(msg.payload):
        pipe_mr = get_pipe_mr(graphql, gl_instance, projects, mr_url)
        if process_possible_branch_change(pipe_mr, msg.payload):
            return

    if not cki_label_changed(msg.payload['changes']):
        LOGGER.info('No CKI label changes to make.')
        return

    if not pipe_mr:
        pipe_mr = get_pipe_mr(graphql, gl_instance, projects, mr_url)
    process_pipe_mr(pipe_mr)


def process_note_event(gl_instance, msg, projects, graphql, **_):
    """Process a note message."""
    if not common.force_webhook_evaluation(msg.payload['object_attributes']['note'],
                                           ['cki', 'ckihook', 'pipeline']):
        LOGGER.info('Note event did not request evaluation, ignoring.')
        return
    mr_url = msg.payload['merge_request']['url']
    LOGGER.info('Processing note event for %s', mr_url)
    pipe_mr = get_pipe_mr(graphql, gl_instance, projects, mr_url)
    process_pipe_mr(pipe_mr)


def process_pipeline_event(gl_instance, msg, projects, graphql, **_):
    """Process a pipeline message."""
    # This should filter out upstream pipeline events and any downstream pipeline events which are
    # not directly related to an MR.
    if not (mr_url := common.get_pipeline_variable(msg.payload, 'mr_url')) or \
       (common.get_pipeline_variable(msg.payload, 'CKI_DEPLOYMENT_ENVIRONMENT',
                                     default='production') != 'production'):
        LOGGER.info('Event did not contain the expected variables, ignoring')
        return

    pipeline_name = common.get_pipeline_variable(msg.payload, 'trigger_job_name')
    LOGGER.info('Processing pipeline event (id: %s, name: %s) for %s',
                msg.payload['object_attributes']['id'], pipeline_name, mr_url)

    pipe_mr = get_pipe_mr(graphql, gl_instance, projects, mr_url)
    process_pipe_mr(pipe_mr)


def process_push_event(gl_instance, msg, projects, graphql, **_):
    """Process a push event message."""
    namespace = msg.payload['project']['path_with_namespace']
    branch_name = msg.payload['ref'].rsplit('/', 1)[-1]
    LOGGER.info('Processing push event for %s on branch %s.', namespace, branch_name)
    if not (branch := projects.get_target_branch(msg.payload['project']['id'], branch_name)):
        LOGGER.info('Ignoring push to unrecognized branch.')
        return
    if not branch.name.endswith('-rt'):
        LOGGER.info('Ignoring push to non-rt branch.')
        return
    retrigger_failed_pipelines(graphql, gl_instance, namespace, branch)


WEBHOOKS = {
    'merge_request': process_mr_event,
    'note': process_note_event,
    'pipeline': process_pipeline_event,
    'push': process_push_event
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('CKIHOOK')
    args = parser.parse_args(args)
    common.generic_loop(args, WEBHOOKS, get_graphql_instance=True)


if __name__ == "__main__":
    main(sys.argv[1:])
