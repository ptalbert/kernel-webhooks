"""Metadata for RHEL projects."""
from dataclasses import dataclass
from dataclasses import field
from dataclasses import fields
from os import environ
import re
from typing import ClassVar

from cki_lib.logger import get_logger
from cki_lib.yaml import load

from webhook.cpc import get_policy_data
from webhook.defs import Label
from webhook.defs import MrScope
from webhook.defs import RH_METADATA_YAML_PATH
from webhook.pipelines import PipelineType

LOGGER = get_logger(__name__)


def check_data(this_dc):
    """Check the dataclass object fields have the right types and are not empty values."""
    dc_type = type(this_dc)
    for dc_field in [field for field in fields(this_dc) if field.init]:
        fvalue = getattr(this_dc, dc_field.name)
        # fields should hold data of the correct type
        if not isinstance(fvalue, dc_field.type):
            raise TypeError(f'{dc_type}.{dc_field.name} must be {dc_field.type}: {this_dc}')
        # do not test for empty values if a field is a bool or has an empty default value
        if isinstance(fvalue, bool) or (isinstance(fvalue, str) and dc_field.default == '') or \
           (isinstance(fvalue, list) and dc_field.default_factory is list):
            continue
        # fields with non-empty default values should not have empty values
        if not fvalue:
            raise ValueError(f'{dc_type}.{dc_field.name} cannot be empty: {this_dc}')


@dataclass(kw_only=True)
class Webhook:
    """Webhook metadata."""

    helpers: ClassVar[dict] = {}

    name: str
    required: bool = True
    label_prefixes: list = field(default_factory=list)
    required_for_qa_scope: MrScope = MrScope.NEEDS_REVIEW
    on_draft: bool = True

    def __post_init__(self):
        """Fix the required_for_qa_scope and Check it."""
        if not isinstance(self.required_for_qa_scope, MrScope):
            self.required_for_qa_scope = MrScope.get(self.required_for_qa_scope)
        check_data(self)

    def status(self, mr_labels):
        """Return the MrScope describing this hook's status for the given set of labels."""
        if not self.required:
            return MrScope.READY_FOR_MERGE
        status = MrScope.NEEDS_REVIEW
        if hook_mr_labels := [label for label in mr_labels if
                              label.scoped and label.prefix in self.label_prefixes]:
            if all(label.scope >= self.required_for_qa_scope for label in hook_mr_labels):
                status = MrScope.READY_FOR_QA
            if all(label.scope >= MrScope.OK for label in hook_mr_labels):
                status = MrScope.READY_FOR_MERGE
        return status


@dataclass(kw_only=True)
class Project:
    # pylint: disable=too-many-instance-attributes
    """Project metadata."""

    id: int  # pylint: disable=invalid-name
    group_id: int
    name: str
    product: str
    pipelines: list = field(default_factory=list, repr=False)
    inactive: bool = False
    sandbox: bool = False
    confidential: bool = False
    group_labels: bool = True
    branches: list = field(default_factory=list, repr=False)
    webhooks: dict = field(default_factory=dict, repr=False)

    def __post_init__(self):
        """Set up branches & pipelines and check data."""
        self.pipelines = [PipelineType.get(p) for p in self.pipelines]
        self.branches = [Branch(**b, project=self) for b in self.branches]
        self.webhooks = {w['name']: Webhook(**w) for w in self.webhooks.values()}
        check_data(self)

    def get_branch_by_name(self, branch_name):
        """Return the branch with the given name, or None."""
        return next((b for b in self.branches if b.name == branch_name), None)

    def get_branches_by_itr(self, itr):
        """Return a list of active branches whose policy matches the given ITR."""
        return [b for b in self.branches if b.internal_target_release == itr and not b.inactive]

    def get_branches_by_ztr(self, ztr):
        """Return a list of active branches whose policy matches the given ZTR."""
        return [b for b in self.branches if b.zstream_target_release == ztr and not b.inactive]

    def webhooks_status(self, mr_labels):
        """Return the minimim MrScope of this project's webhook labels for the given mr_labels."""
        mr_labels = [Label(label) for label in mr_labels]
        return min(webhook.status(mr_labels) for webhook in self.webhooks.values())


@dataclass(eq=True, kw_only=True)
class Branch:
    # pylint: disable=too-many-instance-attributes
    """Branch metadata."""

    name: str
    component: str
    distgit_ref: str
    sub_component: str = ''
    internal_target_release: str = ''
    zstream_target_release: str = ''
    milestone: str = ''
    inactive: bool = False
    pipelines: list = field(default_factory=list, repr=False)
    policy: list = field(default_factory=list, compare=False, repr=False)
    project: Project | None = field(default=None, repr=False)

    def __post_init__(self):
        """Set up pipelines and check data."""
        if self.pipelines:
            self.pipelines[:] = [PipelineType.get(pipe) if isinstance(pipe, str) else pipe for
                                 pipe in self.pipelines]
        if not self.pipelines:
            self.pipelines = self.project.pipelines
        if self.project.inactive:
            self.inactive = True
        check_data(self)

    @property
    def is_alpha(self):
        """Return True if this is an Alpha Branch."""
        return self.stream == 'Alpha'

    @property
    def is_beta(self):
        """Return True if this is an Beta Branch."""
        return self.stream == 'Beta'

    @property
    def major(self):
        """Return the Major version number."""
        return self._parse_version()[0]

    @property
    def minor(self):
        """Return the Minor version number."""
        return self._parse_version()[1]

    @property
    def stream(self):
        """Return the Stream version number."""
        return self._parse_version()[2]

    @property
    def version(self):
        """Return a version string derived from ITR/ZTR, or None."""
        if self.major is None or self.minor is None:
            return None
        return f'{self.major}.{self.minor}'

    def _can_compare(self, other):
        """Do basic comparison checks."""
        if self.component != other.component:
            raise ValueError('Cannot compare Branches from different components.')

    def _parse_version(self):
        """Break up the ITR/ZTR string into its a tuple of its component values."""
        if self.internal_target_release:
            version_string = self.internal_target_release
        elif self.zstream_target_release:
            version_string = self.zstream_target_release
        else:
            return (None, None, None)
        pattern = r'^(?P<major>\d)\.(?P<minor>\d)(?:\.|-)(?P<stream>(?:\d|z|Alpha|Beta))$'
        if result := re.match(pattern, version_string):
            return tuple(int(i) if i.isnumeric() else i for i in result.groups())
        return (None, None, None)

    def __ge__(self, other):
        """Return True if self is greather than or equal to other, otherwise False."""
        self._can_compare(other)
        return self.__gt__(other) or self == other

    def __gt__(self, other):
        """Return True if self is greater than other, otherwise False."""
        self._can_compare(other)
        if other.is_alpha and not self.is_alpha:
            return True
        if other.is_beta and not (self.is_alpha or self.is_beta):
            return True
        return (self.major, self.minor) > (other.major, other.minor)

    def __le__(self, other):
        """Return True if self is less than or equal to other, otherwise False."""
        self._can_compare(other)
        return self.__lt__(other) or self == other

    def __lt__(self, other):
        """Return True if self is less than other, otherwise False."""
        self._can_compare(other)
        if self.is_alpha and not other.is_alpha:
            return True
        if self.is_beta and not (other.is_alpha or other.is_beta):
            return True
        return (self.major, self.minor) < (other.major, other.minor)


class Projects:
    """Projects metadata."""

    def __init__(self, load_policies=False, yaml_path=''):
        """Load yaml into Projects and check data."""
        if not yaml_path:
            yaml_path = environ.get('RH_METADATA_YAML_PATH', RH_METADATA_YAML_PATH)
        raw_data = load(file_path=yaml_path)
        self.projects = {raw_proj['id']: Project(**raw_proj) for raw_proj in raw_data['projects']}
        if load_policies:
            self.do_load_policies()

    @staticmethod
    def _format_project_id(project_id):
        """Return the project_id as an int."""
        # project_id can be a number or a Gitlab ID scalar type, ie. gid://gitlab/Project/1234567
        if isinstance(project_id, str) and project_id.startswith('gid://gitlab/Project/'):
            project_id = project_id.removeprefix('gid://gitlab/Project/')
        return int(project_id)

    def get_project_by_id(self, project_id):
        """Return the project with the given project_id, or None."""
        project_id = self._format_project_id(project_id)
        return self.projects.get(project_id)

    def get_projects_by_name(self, project_name):
        """Return the list of Projects with the given name."""
        return [project for project in self.projects.values() if project.name == project_name]

    def get_target_branch(self, project_id, target_branch):
        """Return the Branch object matching the given project_id and target_branch."""
        project_id = self._format_project_id(project_id)
        if not (project := self.projects.get(project_id)):
            return None
        return next((branch for branch in project.branches if branch.name == target_branch), None)

    def do_load_policies(self):
        """Load policy tokens for each branch."""
        policies = get_policy_data(policy_regex=r'^(rhel-.*|c\d+s)$')
        for project in self.projects.values():
            for branch in project.branches:
                if policy := policies.get(branch.distgit_ref):
                    branch.policy = policy


def is_branch_active(projects, project_id, target_branch):
    """Return True if the target branch is active, otherwise False."""
    if project := projects.get_project_by_id(project_id):
        if branch := project.get_branch_by_name(target_branch):
            return not branch.inactive
    else:
        LOGGER.warning('Project %s is not present in rh_metadata.', project_id)
    return False
