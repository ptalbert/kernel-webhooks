"""Pipeline helper functions."""
from dataclasses import dataclass
from dataclasses import field
from datetime import datetime
from enum import IntEnum
from enum import auto
from functools import cached_property
from os import environ
from re import match as re_match

from cki_lib import logger
from cki_lib.misc import get_nested_key
from reporter.data import CheckoutData
from reporter.settings import JINJA_ENV

from . import defs

LOGGER = logger.get_logger(__name__)
TEMPLATE_FILE = 'utils/failure-only-ckihook.j2'


class PipelineType(IntEnum):
    """Types of Pipelines we might see in our Merge Requests."""

    INVALID = 0
    RHEL = auto()
    CENTOS = auto()
    ARK = auto()
    RHEL_COMPAT = auto()
    REALTIME = auto()
    AUTOMOTIVE = auto()
    _64K = auto()
    DEBUG = auto()

    @property
    def name(self):
        # pylint: disable=function-redefined,invalid-overridden-method
        """Return the name with any leading underscores (_) removed."""
        return getattr(self, '_name_', '').removeprefix('_')

    @classmethod
    def get(cls, name):
        """Return the PipelineType that corresponds to the given job name."""
        if not name:
            return cls.INVALID
        # Try to simply match the pipe_name to the Type name.
        if ptype := next((ptype for ptype in cls if ptype.name == name.upper()), cls.INVALID):
            return ptype
        # If it doesn't have one of these endings then we don't recognize it.
        if not name.endswith(('_merge_request', '_merge_request_private')):
            return cls.INVALID
        # Now match the various types.
        if "_compat_" in name:
            ptype = cls.RHEL_COMPAT
        elif "_debug_" in name:
            ptype = cls.DEBUG
        elif "_realtime_" in name or "_rt_" in name:
            ptype = cls.REALTIME
        elif "_automotive_" in name:
            ptype = cls.AUTOMOTIVE
        elif "_64k_" in name:
            ptype = cls._64K
        elif re_match(r'^c[0-9]+s_', name):
            ptype = cls.CENTOS
        elif re_match(r'^rhel[0-9]+_', name):
            ptype = cls.RHEL
        elif name.startswith('ark_'):
            ptype = cls.ARK
        return ptype

    @property
    def prefix(self):
        """Return the matching CKI label prefix, or None."""
        return CKI_LABEL_PREFIXES[self]


CKI_LABEL_PREFIXES = {PipelineType.INVALID: '',
                      PipelineType.RHEL: 'CKI_RHEL',
                      PipelineType.CENTOS: 'CKI_CentOS',
                      PipelineType.ARK: 'CKI_ARK',
                      PipelineType.RHEL_COMPAT: 'CKI_RHEL',
                      PipelineType.REALTIME: 'CKI_RT',
                      PipelineType.AUTOMOTIVE: 'CKI_Automotive',
                      PipelineType._64K: 'CKI_64k',  # pylint: disable=protected-access
                      PipelineType.DEBUG: 'CKI_Debug'
                      }


class PipelineStatus(IntEnum):
    """Possible status of a pipeline."""

    UNKNOWN = auto()
    MISSING = auto()
    FAILED = auto()
    CANCELED = auto()
    RUNNING = auto()
    PENDING = RUNNING
    CREATED = RUNNING
    OK = auto()
    SUCCESS = OK

    @property
    def title(self):
        """Return capitalized name."""
        return self.name.capitalize() if self.name != 'OK' else 'OK'

    @classmethod
    def get(cls, type_str):
        """Return the PipelineStatus that matches the type_str."""
        return cls.__members__.get(type_str.upper(), cls.UNKNOWN)


@dataclass(frozen=True, kw_only=True, eq=True, order=True)
class PipelineResult:
    # pylint: disable=too-many-instance-attributes
    """Basic pipeline details."""

    # We want to be able to compare results with the same bridge name and sort them by created_at
    # date so we can find the newest instance of the bridge.
    bridge_gid: str = field(compare=False)
    bridge_name: str
    ds_pipeline_gid: str = field(compare=False)
    ds_project_gid: str
    mr_pipeline_gid: str
    ds_namespace: str
    status: PipelineStatus | str = field(compare=False)
    created_at: datetime | str
    allow_failure: bool = field(compare=False)
    stage_data: list = field(compare=False)

    def __post_init__(self):
        """Fix up the type, status, and created_at fields, if needed."""
        if isinstance(self.status, str):
            self.__dict__['status'] = PipelineStatus.get(self.status)
        if isinstance(self.created_at, str) and self.created_at:
            self.__dict__['created_at'] = datetime.fromisoformat(self.created_at[:19])
        # Flatten the stage_data a bit by removing the useless 'nodes' key.
        for stage in self.stage_data:
            if jobs := get_nested_key(stage, 'jobs/nodes'):
                stage['jobs'] = jobs
        LOGGER.info('Created %s', self)

    def __repr__(self):
        """Show yourself."""
        repr_str = f"'{self.bridge_name}' ({self.type.name}), ds ID: {self.ds_pipeline_id}"
        repr_str += f', status: {self.status.name}'
        return f'<Pipeline {repr_str}>'

    @classmethod
    def from_dict(cls, api_dict):
        """Return a new object generated from graphql data."""
        ds_pipe_dict = api_dict.get('downstreamPipeline', {})
        input_dict = {'bridge_gid': api_dict.get('id', ''),
                      'bridge_name': api_dict.get('name', ''),
                      'ds_pipeline_gid': ds_pipe_dict.get('id', ''),
                      'ds_project_gid': get_nested_key(ds_pipe_dict, 'project/id', ''),
                      'mr_pipeline_gid': get_nested_key(api_dict, 'pipeline/id', ''),
                      'ds_namespace': get_nested_key(ds_pipe_dict, 'project/fullPath', ''),
                      'stage_data': get_nested_key(ds_pipe_dict, 'stages/nodes', ''),
                      'status': ds_pipe_dict.get('status', ''),
                      'created_at': api_dict.get('createdAt', ''),
                      'allow_failure': api_dict.get('allowFailure', False),
                      }
        return cls(**input_dict)

    @property
    def bridge_id(self):
        # pylint: disable=invalid-name
        """Return the global ID of the MR bridge job as an int."""
        return int(self.bridge_gid.rsplit('/', 1)[-1]) if self.bridge_gid else 0

    @property
    def ds_pipeline_id(self):
        """Return the downstream pipeline global ID as an int."""
        return int(self.ds_pipeline_gid.rsplit('/', 1)[-1]) if self.ds_pipeline_gid else 0

    @property
    def ds_project_id(self):
        """Return the downstream project global ID as an int."""
        return int(self.ds_project_gid.rsplit('/', 1)[-1]) if self.ds_project_gid else 0

    @property
    def mr_pipeline_id(self):
        """Return the MR pipeline global ID as an int."""
        return int(self.mr_pipeline_gid.rsplit('/', 1)[-1]) if self.mr_pipeline_gid else 0

    @property
    def label(self):
        """Return the label string for pipelines of a known PipelineType, or None."""
        if not (prefix := self.type.prefix):
            return None
        if self.status is PipelineStatus.FAILED and self.allow_failure:
            return f'{prefix}::Warning'
        return f'{prefix}::{self.status.title}::{self.failed_stage.name}' if \
            self.failed_stage else f'{prefix}::{self.status.title}'

    @property
    def failed_stage(self):
        """Return the stages enum value representing the latest failed stage, or None."""
        # If the pipeline isn't failed then there is nothing to do.
        if self.status is not PipelineStatus.FAILED:
            return None
        return next((getattr(self.stages, stage['name']) for stage in reversed(self.stage_data) for
                     job in stage['jobs'] if job['status'].upper() == 'FAILED'), '')

    def get_stage(self, stage_name):
        """Return the stage data matching the given name, or None."""
        return next((stage for stage in self.stage_data if stage['name'] == stage_name), None)

    @cached_property
    def kcidb_data(self):
        """Return the KCIDB reporter CheckoutData for the pipeline, or None."""
        return CheckoutData(f'redhat:{self.ds_pipeline_id}') if self.ds_pipeline_gid else None

    @property
    def kcidb_report(self):
        """Return the templeted results string from KCIDB reporter's CheckoutData, or None."""
        if not self.kcidb_data:
            return None
        with open(TEMPLATE_FILE, encoding='utf-8') as template_file:
            jinja_template = JINJA_ENV.from_string(template_file.read())
        context = {'checkout_data': self.kcidb_data,
                   'build_data': self.kcidb_data.build_data,
                   'test_data': self.kcidb_data.test_data,
                   'is_email': False,
                   'dw_url': environ['DATAWAREHOUSE_URL']}
        return jinja_template.render(context).strip()

    @cached_property
    def stages(self):
        """Return an Enum representing the stages found in the pipeline."""
        return IntEnum('Stages', [stage['name'] for stage in self.stage_data])

    @property
    def type(self):
        """Return the PipelineType derived from the bridge job name."""
        return PipelineType.get(self.bridge_name)

    @property
    def ds_url(self):
        """Return the downstream pipeline URL, or None if it can't be derived."""
        if not self.ds_namespace or not self.ds_pipeline_gid:
            return None
        return f'{defs.GITFORGE}/{self.ds_namespace}/-/pipelines/{self.ds_pipeline_id}'

    @staticmethod
    def prepare_pipelines(input_pipelines):
        """Transform the input pipeline data into objects and filter out old instances."""
        def filter_pipes(new_pipe, existing_pipe):
            """Return whichever Pipeline is the latest, or new_pipe if existing_pipe is None."""
            if not existing_pipe:
                return new_pipe
            latest = existing_pipe if existing_pipe > new_pipe else new_pipe
            LOGGER.debug('Excluding old %s', new_pipe if latest is existing_pipe else existing_pipe)
            return latest

        # Filter out pipelines with no downstream component.
        all_pipelines = [PipelineResult.from_dict(raw_pipeline) for
                         raw_pipeline in input_pipelines if raw_pipeline['downstreamPipeline']]
        pipelines = {}
        for pipe in all_pipelines:
            pipelines[pipe.bridge_name] = filter_pipes(pipe, pipelines.get(pipe.bridge_name, None))
        return list(pipelines.values())
