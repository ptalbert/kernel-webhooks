"""Process bugzilla events from the UMB."""
from collections import defaultdict
from enum import Enum
import json
from os import environ
import sys
from threading import Lock
from threading import Thread
import time

from cki_lib import logger
from cki_lib.gitlab import get_instance
from cki_lib.misc import get_nested_key
from gitlab.exceptions import GitlabListError
from pika.exceptions import AMQPError

from webhook import common
from webhook import defs
from webhook.description import MRDescription
from webhook.graphql import GitlabGraph

LOGGER = logger.get_logger('cki.webhook.umb_bridge')
SEND_DELAY = 1


class MappingType(Enum):
    # pylint: disable=invalid-name
    """The two possible mapping types."""

    Bug = 'bugzilla'
    Jira = 'jira'


def _set_up_send_queue(send_queue, send_message):
    """Set up the send_queue."""
    # Before we do anything check the sender thread is running.
    send_queue.check_status()

    if send_queue.send_function is None:
        LOGGER.info('Setting send_queue.send_message to: %s', send_message)
        send_queue.send_function = send_message


def process_umb_event(body, mapping_data, send_queue, send_message, **_):
    """Process UMB bugzilla events."""
    _set_up_send_queue(send_queue, send_message)

    bug = body['event'].get('bug_id')
    user = body['event'].get('user', {}).get('login')
    if bug is None or user is None:
        LOGGER.warning('No bug_id or user found in UMB event.')
        print(json.dumps(body, indent=2))
        return

    if bug not in mapping_data.bugs:
        LOGGER.info('Ignoring event for unknown bug %d.', bug)
        return

    if user == environ['BUGZILLA_EMAIL']:
        LOGGER.info('Ignoring event by bot user %s.', user)
        return

    LOGGER.info('Event for bug %d by %s relevant to these MRs: %s', bug, user,
                mapping_data.bugs[bug])
    for mrpath in mapping_data.bugs[bug]:
        send_queue.add_bug(mrpath)


def process_jira_event(body, mapping_data, send_queue, send_message, **_):
    """Process Jira webhook events."""
    _set_up_send_queue(send_queue, send_message)

    jissue = get_nested_key(body, 'issue/key', '')
    user = get_nested_key(body, 'user/name')
    LOGGER.info('Processing Jira event for %s by %s', jissue, user)

    if user in defs.JIRA_BOT_ACCOUNTS:
        LOGGER.info('Ignoring event by bot user %s.', user)
        return

    if not jissue.startswith('RHEL-') or jissue not in mapping_data.jiras:
        LOGGER.info("Ignoring event for non-RHEL or untracked issue: '%s'", jissue)
        return

    LOGGER.info('Event for %s relevant to these MRs: %s', jissue, mapping_data.jiras[jissue])

    for mrpath in mapping_data.jiras[jissue]:
        send_queue.add_jira(mrpath)


def process_gitlab_mr(message, mapping_data, **_):
    """Process gitlab MR hook events."""
    action = message.payload['object_attributes'].get('action')
    changes = message.payload['changes']
    if action not in ('open', 'close', 'reopen') and 'description' not in changes:
        LOGGER.info('MR has not changed, ignoring event.')
        return

    current_desc = MRDescription(get_nested_key(message.payload, 'object_attributes/description'),
                                 message.payload['project']['path_with_namespace'],
                                 mapping_data.graphql)
    old_desc = MRDescription(get_nested_key(message.payload, 'changes/description/previous', ''),
                             message.payload['project']['path_with_namespace'],
                             mapping_data.graphql)

    all_current_bzs = current_desc.bugzilla | current_desc.depends
    all_old_bzs = old_desc.bugzilla | old_desc.depends

    bzs_to_unlink = all_current_bzs if action == 'close' else all_old_bzs - all_current_bzs
    bzs_to_link = set() if action == 'close' else all_current_bzs

    all_current_jiras = current_desc.jissue
    all_old_jiras = old_desc.jissue

    jiras_to_unlink = all_current_jiras if action == 'close' else all_old_jiras - all_current_jiras
    jiras_to_link = set() if action == 'close' else all_current_jiras

    mrpath = get_mr_reference(message.payload['object_attributes']['url'])
    mapping_data.link_mr(mrpath, bug_list=bzs_to_link, jira_list=jiras_to_link)
    mapping_data.unlink_mr(mrpath, bug_list=bzs_to_unlink, jira_list=jiras_to_unlink)


class MappingData:
    """Hold mapping of bugs/jiras to MRs."""

    def __init__(self, projects):
        """Connect to GL and build the cache."""
        self.bugs = defaultdict(set)
        self.jiras = defaultdict(set)
        self.logger = logger.get_logger('cki.webhook.umb_bridge.BugData')

        self.gl_instance = get_instance(defs.GITFORGE)
        self.gl_instance.auth()
        self.graphql = GitlabGraph()
        self.projects = projects
        self._build_cache()

    def _build_cache(self):
        """Populate a dictionary of MRs per bug."""
        mr_count = 0

        self.logger.info('Building cache from these projects: %s', self.projects)
        start_t = time.time()
        for project in self.projects:
            gl_project = self.gl_instance.projects.get(project)

            try:
                for gl_mr in gl_project.mergerequests.list(all=True, state='opened', view='simple'):
                    mr_count += 1
                    # Get all the bugs, including dependencies.
                    mr_description = MRDescription(gl_mr.description,
                                                   gl_project.path_with_namespace, self.graphql)
                    bug_list = mr_description.bugzilla | mr_description.depends
                    jira_list = mr_description.jissue

                    mrpath = get_mr_reference(gl_mr.web_url)
                    self.link_mr(mrpath, bug_list, jira_list)
            except GitlabListError as err:
                if err.response_code == 403:
                    self.logger.error("User '%s' does not have access to %s! Skipping.",
                                      self.gl_instance.user.username, project)
                else:
                    raise

        self.logger.info('MRs: %d, Bugs: %d, Jiras: %s. Processing time: %s',
                         mr_count, len(self.bugs), len(self.jiras), time.time() - start_t)

    def _update_mr(self, mapping, mrpath, item_list, action):
        func = getattr(self, action)
        for item in item_list:
            func(mapping, mrpath, item)

    def _link_mr(self, mapping, mrpath, item):
        mapping[item].add(mrpath)
        self.logger.debug('%s added %s.', item, mrpath)

    def link_mr(self, mrpath, bug_list=None, jira_list=None):
        """Insert MR."""
        if bug_list:
            self._update_mr(self.bugs, mrpath, bug_list, '_link_mr')
        if jira_list:
            self._update_mr(self.jiras, mrpath, jira_list, '_link_mr')

    def _unlink_mr(self, mapping, mrpath, item):
        try:
            mapping[item].remove(mrpath)
            self.logger.debug('%s removed from %s.', mrpath, item)
        except KeyError:
            self.logger.warning("%s can't remove %s, not in item's set.", item, mrpath)
        if not mapping[item]:
            del mapping[item]
            self.logger.debug('%s removed from mapping.', item)

    def unlink_mr(self, mrpath, bug_list=None, jira_list=None):
        """Remove MR."""
        if bug_list:
            self._update_mr(self.bugs, mrpath, bug_list, '_unlink_mr')
        if jira_list:
            self._update_mr(self.jiras, mrpath, jira_list, '_unlink_mr')


def get_mr_reference(web_url):
    """Return an MR reference in 'full' format from the given the web_url."""
    web_url_split = web_url.split('/-/')
    namespace = '/'.join(web_url_split[0].split('/')[3:])
    mr_id = web_url_split[-1].split('/')[-1]
    return f'{namespace}!{mr_id}'


MSG_HANDLERS = {'merge_request': process_gitlab_mr,
                'amqp-bridge': process_umb_event,
                defs.JIRA_WEBHOOK_MESSAGE_TYPE: process_jira_event
                }


class SendQueue:
    """Manage queue of MRs to send to the webhooks hook as a separate thread."""

    def __init__(self, sender_exchange, sender_route):
        """Set up send params and spawn thread."""
        self.data = {}
        self.logger = logger.get_logger('cki.webhook.umb_bridge.SendQueue')

        # Must be set to MessageQueue.send_message() method before using the queue.
        self.send_function = None

        # Params passed by self._send_message to send_function, excluding data.
        self.send_params = {'queue_name': sender_route,
                            'exchange': sender_exchange,
                            'headers': {'message-type': defs.UMB_BRIDGE_MESSAGE_TYPE}
                            }

        self._lock = Lock()
        self._thread = Thread(target=self._sender_thread, daemon=True)

    def check_send_function(self):
        """Barf if send_function has not been set."""
        if self.send_function is None:
            self.logger.warning('send_function is not set!')
            return False
        return True

    def add(self, mapping_type, mrpath):
        """Add or update an entry in the queue."""
        if not self.check_send_function():
            return
        with self._lock:
            key = (mapping_type, mrpath)
            self.logger.info('Adding %s from %s.', key[1], key[0].value)
            # Remove it so we can depend on insertion order to test age.
            if key in self.data:
                del self.data[key]
            self.data[key] = time.time()

    def add_bug(self, mrpath):
        """Add or update a Bug entry in the queue."""
        self.add(MappingType.Bug, mrpath)

    def add_jira(self, mrpath):
        """Add or update a Bug entry in the queue."""
        self.add(MappingType.Jira, mrpath)

    def _get(self):
        """Return the oldest item at least SEND_DELAY old."""
        # Caller must hold self.lock!
        if not self.data:
            self.logger.debug('Empty.')
            return None

        # Is the first (oldest) item at least SEND_DELAY old?
        key = next(iter(self.data))
        if self.data[key] > time.time() - SEND_DELAY:
            self.logger.debug('Nothing older than SEND_DELAY (%d) in data.', SEND_DELAY)
            return None
        return key

    def _send_message(self, key):
        """Send a message to the queue."""
        mapping_type, mrpath = key
        try:
            # pylint: disable=not-callable
            params = self.send_params | {'data': {'mrpath': mrpath}}
            params['headers']['source'] = mapping_type.value
            self.send_function(**params)
            self.logger.info('Sent %s from %s.', mrpath, mapping_type.value)
        except AMQPError as err:
            self.logger.error('Error sending message: %s', err)
        del self.data[key]

    def send_all(self):
        """Send all outstanding messages."""
        with self._lock:
            while (key := self._get()) is not None:
                if not self.check_send_function():
                    return
                self._send_message(key)
            self.logger.debug('Nothing to send.')

    def _sender_thread(self):
        """Process send queue."""
        while True:
            time.sleep(10)
            self.send_all()

    def check_status(self):
        """Confirm the sender thread is running, otherwise die."""
        # If the sender thread is not running try to send any messages in the
        # queue and then die.
        if not self._thread.is_alive():
            self.logger.error('Sender thread not running as expected.')
            with self._lock:
                for key in self.data.copy():
                    self._send_message(key)
                sys.exit(1)
        return True

    def start(self):
        """Start the sender thread."""
        self.logger.debug('Starting sender thread.')
        self._thread.start()
        self.check_status()


def route_keys_to_projects(keys):
    """Return a list of project paths given the list of routing keys."""
    projects = []
    for key in keys:
        if key.startswith('gitlab.com.') and key.endswith('.merge_request'):
            projects.append('/'.join(key.split('.')[2:-1]))
    return projects


def main(args):
    """Run main loop."""
    parser_prefix = 'UMB_BRIDGE'
    parser = common.get_arg_parser(parser_prefix)
    parser.add_argument('--rabbitmq-sender-exchange',
                        **common.get_argparse_environ_opts(f'{parser_prefix}_SENDER_EXCHANGE'),
                        help='RabbitMQ Exchange for sending messages.')
    parser.add_argument('--rabbitmq-sender-route',
                        **common.get_argparse_environ_opts(f'{parser_prefix}_SENDER_ROUTE'),
                        help='RabbitMQ Routing Key for sending messages.')
    args = parser.parse_args(args)

    # Convert routing keys to project namespaces.
    projects = route_keys_to_projects(args.rabbitmq_routing_key.split())

    # Initiate and start the send_queue handler.
    send_queue = SendQueue(args.rabbitmq_sender_exchange, args.rabbitmq_sender_route)
    send_queue.start()

    # Populate GL bug/jira mapping and start consuming messages.
    mapping_data = MappingData(projects)
    common.consume_queue_messages(args, MSG_HANDLERS, mapping_data=mapping_data,
                                  send_queue=send_queue, get_gl_instance=False)


if __name__ == "__main__":
    main(sys.argv[1:])
