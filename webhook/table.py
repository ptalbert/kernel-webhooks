"""Table module."""
from dataclasses import dataclass
from dataclasses import field


@dataclass
class Table:
    """Format a table."""

    footnote_list: list = field(default_factory=list)     # A list of footnote strings
    rows: list = field(default_factory=list, init=False)  # The list of TableRow objects

    def __bool__(self):
        """Return True if the Table has rows, otherwise False."""
        return bool(self.rows)

    def __len__(self):
        """Return the number of rows."""
        return len(self.rows)

    def __str__(self):
        """Return the full table markdown."""
        if not self.rows:
            return ''
        row_strings = [str(row) for row in self.rows]
        table_str = ''
        table_str += self.header + '\n' + '\n'.join(row_strings) + '\n'
        if self.footnote_list:
            table_str += self.footnotes
        return table_str + '\n\n'

    def add_row(self, row):
        """Add a TableRow to the Table."""
        if not isinstance(row, TableRow):
            raise TypeError('row must be of TableRow Type.')
        if self.rows:
            if len(self.rows[0]) != len(row):
                raise ValueError('New row is not the same size as existing rows.')
        self.rows.append(row)

    @property
    def columns(self):
        """Return a list of the pretty column names."""
        return [key.replace('_', ' ') for key in self.rows[0].__dict__.keys()] if self.rows else []

    @property
    def footnotes(self):
        """Return a string of the footnotes in markdown list format."""
        footnotes_str = ''
        for index, footnote_str in enumerate(self.footnote_list, start=1):
            footnotes_str += f'{index}. {footnote_str}  \n'
        return footnotes_str

    @property
    def header(self):
        """Return a string of the table header based on the first Row object in markdown format."""
        if not self.rows:
            return ''
        header_str = '|' + '|'.join(self.columns) + '|\n'
        header_str += '|:------' * len(self.columns) + '|'
        return header_str


class TableRow:
    """Format a Table row."""

    def __len__(self):
        """Return the number of columns (attributes)."""
        return len(self.__dict__)

    def __str__(self):
        """Return the Row as markdown."""
        return '|' + '|'.join(self.__dict__.values()) + '|' if self.__dict__ else ''

    def set_value(self, name, value):
        """Set the value of the name attribute."""
        if not hasattr(self, name):
            raise AttributeError(f'{name} is not a valid column name.')
        if format_func := getattr(self, f'_format_{name}', None):
            value = format_func(value)
        if isinstance(value, (list, set)):
            setattr(self, name, '<br>'.join(value))
        elif isinstance(value, dict):
            val = ''
            for index, item in enumerate(value.values(), start=1):
                val += f'{index}. {item}\n'
            setattr(self, name, val)
        else:
            setattr(self, name, value)
