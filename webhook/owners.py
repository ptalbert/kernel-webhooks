"""Parser for the RHEL owners.yaml file."""

import fnmatch
import re

from cki_lib.misc import get_nested_key
import yaml


class Entry:
    """Represents an entry in the owners.yaml file."""

    def __init__(self, entry):
        """Save an individual entry from the Parser class."""
        self._entry = entry

    @property
    def subsystem_name(self):
        """Return the subsystem name as a string."""
        return get_nested_key(self._entry, 'subsystem', '')

    @property
    def subsystem_label(self):
        """Return the subsystem label as a string."""
        return get_nested_key(self._entry, 'labels/name', '')

    @property
    def ready_for_merge_label_deps(self):
        """Return the list of ready for merge dep labels, if any."""
        return get_nested_key(self._entry, 'labels/readyForMergeDeps') or []

    @property
    def status(self):
        """Return the status."""
        return get_nested_key(self._entry, 'status', '')

    @property
    def required_approvals(self):
        """Return the requiredApproval value if set, otherwise False."""
        return get_nested_key(self._entry, 'requiredApproval', False)

    @property
    def maintainers(self):
        """Return the list of maintainers."""
        if not (users := get_nested_key(self._entry, 'maintainers')):
            users = []
        return list(_flatten(users))

    @property
    def reviewers(self):
        """Return the list of reviewers."""
        if not (users := get_nested_key(self._entry, 'reviewers')):
            users = []
        return list(_flatten(users))

    @property
    def scm(self):
        """Return the SCM string."""
        return get_nested_key(self._entry, 'scm', '')

    @property
    def mailing_list(self):
        """Return the mailinglist."""
        return get_nested_key(self._entry, 'mailingList', '')

    @property
    def devel_sst(self):
        """Return the development subsystem team."""
        return get_nested_key(self._entry, 'devel-sst', '')

    @property
    def qe_sst(self):
        """Return the QE subsystem team."""
        return get_nested_key(self._entry, 'qe-sst', '')

    @property
    def path_include_regexes(self):
        """Return the list of include path regexes."""
        if not (paths := get_nested_key(self._entry, 'paths/includeRegexes')):
            paths = []
        return paths

    @property
    def path_includes(self):
        """Return the list of include paths."""
        if not (paths := get_nested_key(self._entry, 'paths/includes')):
            paths = []
        return paths

    @property
    def path_excludes(self):
        """Return the list of exclude paths."""
        if not (paths := get_nested_key(self._entry, 'paths/excludes')):
            paths = []
        return paths

    def _perform_match(self, filename):
        includes_match = _glob_matches(filename, self.path_includes) or \
                         _regex_matches(filename, self.path_include_regexes)
        return includes_match and not _glob_matches(filename, self.path_excludes)

    def matches(self, filenames):
        """Determine if any of the filenames match the path includes and excludes."""
        return any(self._perform_match(x) for x in filenames)

    def __repr__(self):
        """Return the subsystem name as the string repesentation of the Entry class."""
        return self.subsystem_name


class Parser:
    """YAML parser for the owners.yaml file."""

    def __init__(self, owner_yaml_contents):
        """Parse the string contents of the owners.yaml file."""
        if not owner_yaml_contents:
            self._entries = []
        else:
            owners = yaml.load(owner_yaml_contents, Loader=yaml.SafeLoader)
            self._entries = [Entry(entry) for entry in owners['subsystems']]

    def get_all_entries(self):
        """Return all of the entries in the owners.yaml file."""
        return self._entries

    def get_matching_entries(self, filenames):
        """Return all of the matching entries in the owners.yaml file."""
        return [x for x in self._entries if x.matches(filenames)]

    def get_matching_entries_by_label(self, label):
        """Return all the entries matching the given label name."""
        return [x for x in self._entries if x.subsystem_label == label]


def _regex_matches(filename, regexes):
    """Perform a regex match against multiple patterns."""
    return any(_regex_match(filename, x) for x in regexes)


def _regex_match(filename, regex):
    if not regex:
        return True

    return re.compile(regex).search(filename) is not None


def _glob_matches(filename, patterns):
    """Perform a glob match against multiple patterns."""
    return any(glob_match(filename, x) for x in patterns)


def glob_match(filename, pattern):
    """Check whether the filename matches the given shell-style pattern.

    Unlike fnmatch.fnmatchcase, the directories are matched correctly.
    """
    # Special case: a pattern ending with a slash matches everything under
    # the given directory.
    dir_match = False
    if pattern.endswith('/'):
        pattern = pattern[:-1]
        dir_match = True

    # Split to path components.
    fn_components = filename.split('/')
    pat_components = pattern.split('/')

    # If the pattern has more components, there is no way the filename can
    # match.
    if len(fn_components) < len(pat_components):
        return False

    # Check the path components one by one.
    for fn_one, pat_one in zip(fn_components, pat_components):
        if not fnmatch.fnmatchcase(fn_one, pat_one):
            return False

    # If we're matching everything under the pattern directory, it's okay if
    # there are extra components in the path. This is a match.
    if dir_match:
        return True

    # We may have more components in the filename than in the pattern. This
    # should generally not happen. Let's be conservative and return no
    # match in such case.
    return len(fn_components) == len(pat_components)


def _flatten(lst):
    for i in lst:
        if isinstance(i, list):
            yield from _flatten(i)
        else:
            yield i
