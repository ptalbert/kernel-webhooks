labels:
 - name: readyForMerge
   color: '#8BCA42'
   description: "This MR is waiting to be merged by a maintainer."
   devaction:
 - name: readyForQA
   color: "#CAC542"
   description: "This MR is waiting for QA to complete testing.  Be patient and wait for QE to test by the BZs' ITM deadline."
   devaction:
 - name: Acks::Blocked
   color: '#FF0000'
   description: "This MR is blocked by unresolved threads."
   devaction: "View the MR and answer all questions and comments in unresolved threads."
 - name: Acks::OK
   color: '#428BCA'
   description: "This MR has been approved by reviewers."
   devaction:
 - name: Acks::NACKed
   color: '#FF0000'
   description: "This MR has been Nacked by reviewers."
   devaction: "View The MR and answer the reviewer's questions and comments."
 - name: Acks::NeedsReview
   color: '#FF0000'
   description: "This MR needs more approvals from reviewers."
   devaction: "If this MR requires urgent reviews, @-mention the outstanding reviewers in the MR with a polite request for reviews.  Otherwise, be patient for reviews."
 - name: Bugzilla::OK
   color: '#428BCA'
   description: "The BZs in this MR have valid state for this release."
   devaction:
 - name: Bugzilla::NeedsTesting
   color: '#CAC542'
   description: "This MR is waiting for QE to test.  QE will add 'Verified:Tested' to each BZ and any dependent BZs as they complete testing."
   devaction: "Monitor this MR's BZs and dependencies for 'Verified:Tested' status."
 - name: Bugzilla::NeedsReview
   color: '#FF0000'
   description: "This MR has BZs that are not in a valid state for merge."
   devaction: "Verify the BZs statuses, flags, and fields."
 - name: Bugzilla::Closed
   color: '#FF0000'
   description: "This MR has BZs that have been CLOSED."
   devaction: "Verify the BZs statuses.  Reopen the BZs, use another BZ, or close the MR."
 - name: Bugzilla::Missing
   color: '#FF0000'
   description: "This MR has one or more of the MR commits and/or the MR description missing a Bugzilla: entry."
   devaction: "Confirm that the commit messages and MR description conform to https://red.ht/kwf_commit_rules."
 - name: Bugzilla::Invalid
   color: '#FF0000'
   description: "This MR has one or more BZs that are not valid -- they do not exist."
   devaction: "Verify the BZ numbers are valid.  Adjust them to BZ numbers that exist."
 - name: CommitRefs::OK
   color: '#428BCA'
   description: "All commits in this MR have valid upstream or RHEL-only commit references."
   devaction:
 - name: CommitRefs::Missing
   color: '#FF0000'
   description: "This MR contains commits without sufficient upstream or RHEL-only attribution."
   devaction: "Verify the 'Upstream-status:' tag and/or commit reference lines in all commits conform to https://red.ht/kwf_commit_rules."
 - name: CommitRefs::NeedsReview
   color: '#FF0000'
   description: "This MR contains commits with improperly formatted upstream attribution."
   devaction: "Verify the commit reference lines in all commits conform to https://red.ht/kwf_commit_rules."
 - name: Configuration
   color: '#428BCA'
   description: "Indicates an issue or MR is related to kernel configurations."
   devaction:
 - name: "CKI::Missing"
   color: '#FF0000'
   description: "One or more of the required CKI pipelines for this MR is missing."
   devaction:
 - name: "CKI::Failed"
   color: '#FF0000'
   description: "This MR's overall CKI pipeline status is Failed."
   devaction:
 - name: "CKI::Canceled"
   color: '#FF0000'
   description: "One or more of the required CKI pipelines for this MR has been canceled."
   devaction:
 - name: "CKI::Running"
   color: '#CAC542'
   description: "One or more of the required CKI pipelines for this MR is still running."
   devaction:
 - name: "CKI::OK"
   color: '#428BCA'
   description: "All the required CKI pipelines for this MR have passed."
   devaction:
 - name: "^CKI_(RHEL|CentOS|ARK|RT|Automotive|64k|Debug)::OK$"
   color: '#428BCA'
   description: "This MR's latest CKI_%s pipeline was successful."
   devaction:
   regex: True
 - name: "^CKI_(RHEL|CentOS|ARK|RT|Automotive|64k|Debug)::Canceled$"
   color: '#FF0000'
   description: "This MR's latest CKI_%s pipeline was canceled."
   devaction:
   regex: True
 - name: "^CKI_(RHEL|CentOS|ARK|RT|Automotive|64k|Debug)::Missing$"
   color: '#FF0000'
   description: "This MR is missing the expected CKI_%s pipeline."
   devaction:
   regex: True
 - name: "^CKI_(RHEL|CentOS|ARK|RT|Automotive|64k|Debug)::Running$"
   color: '#CAC542'
   description: "This MR's latest CKI_%s pipeline is still running."
   devaction:
   regex: True
 - name: "^CKI_(RHEL|CentOS|ARK|RT|Automotive|64k|Debug)::Unknown$"
   color: '#FF0000'
   description: "This MR's latest CKI_%s pipeline is in an unexpected state."
   devaction:
   regex: True
 - name: "^CKI_(RHEL|CentOS|ARK|RT|Automotive|64k|Debug)::Warning$"
   color: '#6242ca'
   description: "This MR's latest non-blocking CKI_%s pipeline has failed."
   devaction:
   regex: True
 - name: "^CKI_(RHEL|CentOS|ARK|RT|Automotive|64k|Debug)::Failed::prepare$"
   color: '#FF0000'
   description: "This MR's latest CKI_%s pipeline failed at the 'prepare' stage."
   devaction:
   regex: True
 - name: "^CKI_(RHEL|CentOS|ARK|RT|Automotive|64k|Debug)::Failed::merge$"
   color: '#FF0000'
   description: "This MR's latest CKI_%s pipeline failed at the 'merge' stage."
   devaction: "The MR's code changes failed to apply.  Fix the changes and resubmit the MR."
   regex: True
 - name: "^CKI_(RHEL|CentOS|ARK|RT|Automotive|64k|Debug)::Failed::build$"
   color: '#FF0000'
   description: "This MR's latest CKI_%s pipeline failed at the 'build' stage."
   devaction: "The MR's code changes failed to compile.  Fix the changes and resubmit the MR."
   regex: True
 - name: "^CKI_(RHEL|CentOS|ARK|RT|Automotive|64k|Debug)::Failed::build-tools$"
   color: '#FF0000'
   description: "This MR's latest CKI_%s pipeline failed at the 'build-tools' stage."
   devaction:
   regex: True
 - name: "^CKI_(RHEL|CentOS|ARK|RT|Automotive|64k|Debug)::Failed::publish$"
   color: '#FF0000'
   description: "This MR's latest CKI_%s pipeline failed at the 'publish' stage."
   devaction:
   regex: True
 - name: "^CKI_(RHEL|CentOS|ARK|RT|Automotive|64k|Debug)::Failed::setup$"
   color: '#FF0000'
   description: "This MR's latest CKI_%s pipeline failed at the 'setup' stage."
   devaction:
   regex: True
 - name: "^CKI_(RHEL|CentOS|ARK|RT|Automotive|64k|Debug)::Failed::test$"
   color: '#FF0000'
   description: "This MR's latest CKI_%s pipeline failed at the 'test' stage."
   devaction:
   regex: True
 - name: "^CKI_(RHEL|CentOS|ARK|RT|Automotive|64k|Debug)::Failed::wait-for-triage$"
   color: '#FF0000'
   description: "This MR's latest CKI_%s pipeline failed at the 'wait-for_triage' stage."
   devaction:
   regex: True
 - name: "^CKI_(RHEL|CentOS|ARK|RT|Automotive|64k|Debug)::Failed::kernel-results$"
   color: '#FF0000'
   description: "This MR's latest CKI_%s pipeline failed at the 'kernel-results' stage."
   devaction:
   regex: True
 - name: CKI_RT::Waived
   color: '#6242ca'
   description: "This MR's latest CKI_RT pipeline has been waived by a maintainer."
   devaction:
 - name: Dependencies::OK
   color: '#428BCA'
   description: "This MR does not depend on any other MR."
   devaction:
 - name: "^Dependencies::OK::([a-f0-9]{12})$"
   color: '#428BCA'
   description: "This MR does not depend on any other MR."
   devaction:
   regex: True
 - name: External Contribution
   color: '#FF0000'
   description: "This MR was submitted by an external contributor."
   devaction:
 - name: JIRA::OK
   color: '#428BCA'
   description: "The JIRA Issues in this MR have valid state for this release."
   devaction:
 - name: JIRA::NeedsTesting
   color: '#CAC542'
   description: "This MR is waiting for QE to test.  QE will add 'Verified:Tested' to each JIRA Issue and any dependent Issues as they complete testing."
   devaction: "Monitor this MR's JIRA Issues and dependencies for 'Verified:Tested' status."
 - name: JIRA::NeedsReview
   color: '#FF0000'
   description: "This MR has JIRA Issues that are not in a valid state for merge."
   devaction: "Verify the JIRA Issues statuses, flags, and fields."
 - name: JIRA::Closed
   color: '#FF0000'
   description: "This MR has JIRA Issues that have been CLOSED."
   devaction: "Verify the JIRA Issues statuses.  Reopen the Issues, use another Issue, or close the MR."
 - name: JIRA::Missing
   color: '#FF0000'
   description: "This MR has one or more of the MR commits and/or the MR description missing a JIRA: entry."
   devaction: "Confirm that the commit messages and MR description conform to https://red.ht/kwf_commit_rules."
 - name: JIRA::Invalid
   color: '#FF0000'
   description: "This MR has one or more JIRA Issues that are not valid -- they do not exist."
   devaction: "Verify the JIRA Issue numbers are valid.  Adjust them to Issue numbers that exist."
 - name: KABI
   color: '#EEE600'
   description: "This MR contains commits that may impact kernel ABI."
   devaction:
 - name: Limited CI::OK
   color: '#428BCA'
   description: "Status of latest limited CI run."
   devaction:
 - name: Limited CI::canceled
   color: '#CAC542'
   description: "Status of latest limited CI run."
   devaction:
 - name: Limited CI::failed::prepare
   color: '#FF0000'
   description: "Status of latest limited CI run."
   devaction:
 - name: Limited CI::failed::build
   color: '#FF0000'
   description: "Status of latest limited CI run."
   devaction:
 - name: Limited CI::failed::merge
   color: '#FF0000'
   description: "Status of latest limited CI run."
   devaction:
 - name: Limited CI::pending
   color: '#CAC542'
   description: "Status of latest limited CI run."
   devaction:
 - name: Limited CI::pending::kernel-results
   color: '#CAC542'
   description: "Status of latest limited CI run."
   devaction:
 - name: Limited CI::running
   color: '#CAC542'
   description: "Status of latest limited CI run."
   devaction:
 - name: Limited CI::created
   color: '#CAC542'
   description: "Status of latest limited CI run."
   devaction:
 - name: Limited CI::running::kernel-results
   color: '#CAC542'
   description: "Status of latest limited CI run."
 - name: Limited CI::running::build
   color: '#CAC542'
   description: "Status of latest limited CI run."
 - name: Limited CI::running::merge
   color: '#CAC542'
   description: "Status of latest limited CI run."
 - name: Limited CI::success
   color: '#428BCA'
   description: "Status of latest limited CI run."
   devaction:
 - name: Merge::OK
   color: '#428BCA'
   description: "This Merge Request merges cleanly into it's target branch."
   devaction:
 - name: Merge::Conflicts
   color: '#FF0000'
   description: "This Merge Request can not be merged cleanly into it's target branch, due to conflicts with other recently merged code."
   devaction: "The Merge Request submitter must rebase their work on the latest version of their target branch and force-push an update."
 - name: Merge::Warning
   color: '#CAC542'
   description: "This Merge Request has conflicts with other pending MRs."
   devaction: "Coordinate with the authors of the conflicting MRs to determine the best solution to allow all code to merge cleanly."
 - name: Reviewers::Wanted
   color: '#CC338B'
   description: "This Merge Request does not have many reviewers assigned, and wants attention from reviewers not explicitly assigned to it."
   devaction: "If this MR requires urgent reviews, @-mention the outstanding reviewers in the MR    with a polite request for reviews.  Otherwise, be patient for reviews."
 - name: Signoff::OK
   color: '#428BCA'
   description: "The author has acknowledged the DCO (https://developercertificate.org)."
   devaction:
 - name: Signoff::NeedsReview
   color: '#FF0000'
   description: "MR description or commits lack proper Signed-off-by: lines."
   devaction: "Confirm MR description and commits all have a Signed-off-by: tag as specified in https://red.ht/kwf_commit_rules."
 - name: TargetedTestingMissing
   color: '#6242ca'
   description: "This MR's head pipeline does not include any Targeted Tests."
   devaction:
 - name: CWF::Incident::Active
   color: '#FF0000'
   description: "This incident is significantly affecting the production environment."
 - name: CWF::Incident::Mitigated
   color: '#CAC542'
   description: "This incident is affecting the production environment in a limited way."
 - name: CWF::Incident::Resolved
   color: '#428BCA'
   description: "This incident is no longer affecting the production environment. Some work remains to be done, e.g. further monitoring, documentation or root cause remediation."
 - name: CWF::Issue::OK
   color: '#428BCA'
   description: "This MR is linked to a GitLab issue."
   devaction:
 - name: CWF::Issue::Missing
   color: '#FF0000'
   description: "This MR is missing a link to a GitLab issue."
   devaction: "Confirm that the MR description contains a link to a GitLab issue."
 - name: "^CWF::Sprint::(.*)$"
   color: '#AAAAAA'
   description: "This issue was closed during %s."
   regex: true
 - name: CWF::Type::ARK
   color: '#428BCA'
   description: "This issue belongs to the ARK team."
 - name: CWF::Type::CKI
   color: '#428BCA'
   description: "This issue belongs to the CKI team."
 - name: CWF::Type::KWF
   color: '#428BCA'
   description: "This issue belongs to the KWF team."
 - name: CWF::Type::Incident
   color: '#428BCA'
   description: "This issue represents an incident."
 - name: "^Acks::(\\w[\\w\\-]*)::NACKed$"
   color: '#FF0000'
   description: "This merge request has been explicitly Nacked by Red Hat engineering."
   devaction: "View the MR and discuss the reasons for the rejection."
   regex: True
   regex_field_id: 2
 - name: "^Acks::(\\w[\\w\\-]*)::NeedsReview$"
   color: '#FF0000'
   description: "This MR needs more approvals from the reviewers in the %s group."
   devaction: "If this MR requires urgent reviews, @-mention the outstanding reviewers in the MR with a polite request for reviews.  Otherwise, be patient for reviews."
   regex: True
   regex_field_id: 2
 - name: "^Acks::(\\w[\\w\\-]*)::OK$"
   color: '#428BCA'
   description: "This MR has been approved by reviewers in the %s group."
   devaction:
   regex: True
   regex_field_id: 2
 - name: "^Subsystem:(\\w[\\w\\-]*)$"
   color: '#778899'
   description: "An MR that affects code related to %s."
   devaction:
   regex: True
   regex_field_id: 2
 - name: "^Dependencies::([a-f0-9]{12})$"
   color: '#FF0000'
   description: "This MR depends on code which does not exist in the target branch."
   devaction:
   regex: True
   regex_field_id: 2
 - name: "^CodeChanged::v(\\d+)$"
   color: '#013220'
   description: "Code changed in v%s of this Merge Request."
   devaction:
   regex: True
 - name: "^ExternalCI::(.*)::OK$"
   color: '#428BCA'
   description: "The subsystem-required %s testing has been completed."
   devaction:
   regex: True
 - name: "^ExternalCI::(.*)::Waived$"
   color: '#428BCA'
   description: "The subsystem-required %s testing has been waived."
   devaction:
   regex: True
 - name: "^ExternalCI::(.*)::NeedsTesting$"
   color: '#CAC542'
   description: "The subsystem-required %s testing has not yet been completed."
   devaction: "Wait for testing to complete."
   regex: True
 - name: "^ExternalCI::(.*)::Failed$"
   color: '#ff0000'
   description: "The subsystem-required %s testing has failed."
   devaction: "There was a problem uncovered by subsystem testing, please consult the relevant subsystem testing team."
   regex: True
 - name: "ExternalCI::OK"
   color: '#428BCA'
   description: "All the subsystem-required testing has been completed."
   devaction:
 - name: "ExternalCI::NeedsTesting"
   color: '#CAC542'
   description: "One or more of the subsystem-required testing has not yet been completed."
   devaction: "Wait for testing to complete."
 - name: "ExternalCI::Failed"
   color: '#ff0000'
   description: "One or more of the subsystem-required testing has failed."
   devaction: "There was a problem uncovered by subsystem testing, please consult the relevant subsystem testing team."
