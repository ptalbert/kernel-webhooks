"""Webhook interaction tests."""
from datetime import datetime
from subprocess import CalledProcessError
from subprocess import CompletedProcess
from unittest import TestCase
from unittest import mock

from webhook import defs
from webhook import mergehook


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestMergehook(TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._mocked_runs = []
        self._mocked_calls = []

    def _mock_run(self, args, *, check=False, **_):
        for run in self._mocked_runs:
            if run[0] == args:
                (returncode, stdout) = run[1:]
                break
        else:
            self.fail(f'Command {args} not found in mocked subprocess.run')
        self._mocked_calls.append(args)
        if returncode:
            raise CalledProcessError(returncode, args, output=stdout)
        return CompletedProcess(args, returncode, stdout=stdout)

    def _add_run_result(self, args, returncode, stdout=None):
        self._mocked_runs.append((args, returncode, stdout))

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_git_branch_copy(self):
        location = '/src/kernel-ark/'
        branch = 'os-build'
        with self.assertLogs('cki.webhook.mergehook', level='DEBUG') as logs:
            mergehook._git_branch_copy(location, branch)
            self.assertIn(f'git branch --copy {branch}', logs.output[-1])

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_git_reset(self):
        location = '/src/kernel-ark/'
        branch = 'os-build'
        with self.assertLogs('cki.webhook.mergehook', level='DEBUG') as logs:
            mergehook._git_reset(location, branch)
            self.assertIn(f'git reset --hard {branch}', logs.output[-1])

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_git_merge(self):
        location = '/src/kernel-ark/'
        reference = 'origin/merge-requests/66'
        with self.assertLogs('cki.webhook.mergehook', level='DEBUG') as logs:
            mergehook._git_merge(location, reference)
            self.assertIn(f'git merge --quiet --no-edit {reference}', logs.output[-1])

    @mock.patch('subprocess.run', mock.Mock(return_value=True))
    def test_git_branch_delete(self):
        location = '/src/kernel-ark/'
        branch = 'os-build'
        with self.assertLogs('cki.webhook.mergehook', level='DEBUG') as logs:
            mergehook._git_branch_delete(location, branch)
            self.assertIn(f'git branch -D {branch}', logs.output[-1])

    @mock.patch('webhook.mergehook._git_reset', mock.Mock(return_value=True))
    def test_mr_not_mergeable(self):
        worktree_dir = '/src/kernel-ark-temp-merge-branch/'
        target_branch = 'kernel-ark/os-build'
        merge_branch = 'temp-merge-branch'
        m_args = ['git', 'merge', '--quiet', '--no-edit', merge_branch]
        self._add_run_result(m_args, 5, 'Catastrophic error!')
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            ret = mergehook.mr_not_mergeable(worktree_dir, target_branch, merge_branch)
            self.assertTrue(ret)

    @mock.patch('webhook.mergehook._git_reset', mock.Mock(return_value=True))
    def test_mr_not_mergeable_false(self):
        worktree_dir = '/src/kernel-ark-temp-merge-branch/'
        target_branch = 'kernel-ark/os-build'
        merge_branch = 'temp-merge-branch'
        m_args = ['git', 'merge', '--quiet', '--no-edit', merge_branch]
        self._add_run_result(m_args, 0)
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            ret = mergehook.mr_not_mergeable(worktree_dir, target_branch, merge_branch)
            self.assertFalse(ret)

    def test_fetch_mr_list(self):
        mock_proj = mock.Mock()
        mock_proj.path_with_namespace = 'cki-project/kernel-ark'
        mock_mr = mock.Mock()
        mock_mr.target_branch = 'os-build'
        mock_graphql = mock.Mock()
        mr_list = [{'iid': '876',
                    'author': {'username': 'shadowman'},
                    'title': 'This is a test of the emergency mergecast system',
                    'targetBranch': 'os-build',
                    'project': {'fullPath': 'cki-project/kernel-ark'}
                    }]
        qres = {'project':
                {'id': 'gid://gitlab/Project/13604247',
                 'mergeRequests':
                 {'pageInfo': {'hasNextPage': False, 'endCursor': 'xyz'},
                  'nodes': mr_list
                  }
                 }
                }
        mock_graphql.check_query_results.return_value = qres
        result = mergehook.fetch_mr_list(mock_proj, mock_mr, mock_graphql)
        self.assertEqual(result, mr_list)

    @mock.patch('webhook.mergehook.create_worktree_timestamp', mock.Mock(return_value=True))
    @mock.patch('os.path.exists', mock.Mock(return_value=False))
    @mock.patch('webhook.mergehook.clean_up_temp_merge_branch')
    def test_handle_stale_worktree(self, clean_up):
        rhkernel_src = '/src/kernel'
        merge_branch = 'kernel-ark/os-build'
        worktree_dir = '/tmp/worktree'
        clean_up.return_value = True

        m_args = ['git', 'branch', '-D', f'{merge_branch}-save']
        self._add_run_result(m_args, 4, 'Uhhh yeah it exploded')
        with mock.patch('builtins.open', mock.mock_open(read_data='20200504112233')) as m:
            with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
                mergehook.handle_stale_worktree(rhkernel_src, merge_branch, worktree_dir)
            m.assert_called_once_with(f'{worktree_dir}/timestamp', 'r', encoding='ascii')
            clean_up.assert_called_once()

        timestamp = datetime.now().strftime('%Y%m%d%H%M%S')
        clean_up.call_count = 0
        with mock.patch('builtins.open', mock.mock_open(read_data=timestamp)) as m2:
            with self.assertRaises(RuntimeError):
                mergehook.handle_stale_worktree(rhkernel_src, merge_branch, worktree_dir)
            m2.assert_called_once_with(f'{worktree_dir}/timestamp', 'r', encoding='ascii')
            clean_up.assert_not_called()

    def test_create_worktree_timestamp(self):
        worktree_dir = '/tmp/worktree'
        with mock.patch('builtins.open', mock.mock_open()) as m:
            mergehook.create_worktree_timestamp(worktree_dir)
            m.assert_called_once_with(f'{worktree_dir}/timestamp', 'w', encoding='ascii')
            handle = m()
            handle.write.assert_called_once()

    @mock.patch('webhook.mergehook.fetch_mr_list')
    @mock.patch('webhook.mergehook._git_branch_copy', mock.Mock(return_value=True))
    @mock.patch('webhook.mergehook.mr_not_mergeable', mock.Mock(return_value=False))
    @mock.patch('webhook.mergehook._git_reset', mock.Mock(return_value=True))
    @mock.patch('webhook.mergehook._git_branch_delete', mock.Mock(return_value=True))
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_check_for_other_merge_conflicts_ok(self, mock_fmrl):
        mock_proj = mock.Mock()
        mock_proj.name = 'kernel-ark'
        mock_mr = mock.Mock()
        mock_mr.iid = 66
        mock_gql = mock.Mock()
        merge_branch = 'temp-merge-branch'
        worktree_dir = '/src/kernel-ark-temp-merge-branch/'
        mr_list = [{'iid': '44',
                    'author': {'username': 'shadowman'},
                    'title': 'Update the kernel release name',
                    'targetBranch': 'os-build',
                    'project': {'fullPath': 'cki-project/kernel-ark'},
                    'labels': {'nodes': [{'title': 'blah'}]}
                    },
                   {'iid': '55',
                    'author': {'username': 'joedev'},
                    'title': 'A terrible misguided commit without Signof-by',
                    'targetBranch': 'os-build',
                    'project': {'fullPath': 'cki-project/kernel-ark'},
                    'labels': {'nodes': [{'title': 'blah'}]}
                    },
                   {'iid': '66',
                    'author': {'username': 'rhnewbie'},
                    'title': 'My first MR!',
                    'targetBranch': 'os-build',
                    'project': {'fullPath': 'cki-project/kernel-ark'},
                    'labels': {'nodes': [{'title': 'blah'}]}
                    }
                   ]
        mock_fmrl.return_value = mr_list
        m44_args = ['git', 'merge', '--quiet', '--no-edit',
                    f"{mock_proj.name}/merge-requests/{mr_list[0]['iid']}"]
        m55_args = ['git', 'merge', '--quiet', '--no-edit',
                    f"{mock_proj.name}/merge-requests/{mr_list[1]['iid']}"]
        m66_args = ['git', 'merge', '--quiet', '--no-edit',
                    f"{mock_proj.name}/merge-requests/{mr_list[1]['iid']}"]
        self._add_run_result(m44_args, 0)
        self._add_run_result(m55_args, 0)
        self._add_run_result(m66_args, 0)
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            ret = mergehook.check_for_other_merge_conflicts(mock_proj, mock_mr, mock_gql,
                                                            merge_branch, worktree_dir)
            self.assertEqual(ret, [])

    @mock.patch('webhook.mergehook.mr_not_mergeable')
    @mock.patch('webhook.mergehook.fetch_mr_list')
    @mock.patch('webhook.mergehook._git_branch_copy', mock.Mock(return_value=True))
    @mock.patch('webhook.mergehook._git_reset', mock.Mock(return_value=True))
    @mock.patch('webhook.mergehook._git_branch_delete', mock.Mock(return_value=True))
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_check_for_other_merge_conflicts_bad(self, mock_fmrl, mock_nm):
        mock_proj = mock.Mock()
        mock_proj.name = 'kernel-ark'
        mock_mr = mock.Mock()
        mock_mr.iid = 66
        mock_gql = mock.Mock()
        merge_branch = 'temp-merge-branch'
        worktree_dir = '/src/kernel-ark-temp-merge-branch/'

        mr_list = [{'iid': '44',
                    'author': {'username': 'shadowman'},
                    'title': 'Update the kernel release name',
                    'targetBranch': 'os-build',
                    'project': {'fullPath': 'cki-project/kernel-ark'},
                    'labels': {'nodes': [{'title': defs.MERGE_CONFLICT_LABEL}]}
                    }]
        mock_fmrl.return_value = mr_list
        m_args = ['git', 'merge', '--quiet', '--no-edit',
                  f"{mock_proj.name}/merge-requests/{mr_list[0]['iid']}"]

        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            ret = mergehook.check_for_other_merge_conflicts(mock_proj, mock_mr, mock_gql,
                                                            merge_branch, worktree_dir)
            self.assertNotIn(m_args, self._mocked_calls)
            self.assertEqual(ret, [])

        mr_list = [{'iid': '44',
                    'author': {'username': 'shadowman'},
                    'title': 'Update the kernel release name',
                    'targetBranch': 'os-build',
                    'project': {'fullPath': 'cki-project/kernel-ark'},
                    'labels': {'nodes': [{'title': 'blah'}]}
                    }]
        error_text = (f"MR !{mr_list[0]['iid']} from @{mr_list[0]['author']['username']} "
                      f"(`{mr_list[0]['title']}`) conflicts with this MR.  \n")
        mock_fmrl.return_value = mr_list
        m_args = ['git', 'merge', '--quiet', '--no-edit',
                  f"{mock_proj.name}/merge-requests/{mr_list[0]['iid']}"]

        mock_nm.return_value = True
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            ret = mergehook.check_for_other_merge_conflicts(mock_proj, mock_mr, mock_gql,
                                                            merge_branch, worktree_dir)
            self.assertNotIn(m_args, self._mocked_calls)
            self.assertEqual(ret, [])

        mock_nm.return_value = False
        self._add_run_result(m_args, 5, 'Catastrophic error!')
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            ret = mergehook.check_for_other_merge_conflicts(mock_proj, mock_mr, mock_gql,
                                                            merge_branch, worktree_dir)
            self.assertIn(m_args, self._mocked_calls)
            self.assertEqual(ret, [error_text, 'Catastrophic error!'])

    @mock.patch('webhook.mergehook.create_worktree_timestamp', mock.Mock(return_value=True))
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_check_for_merge_conflicts_ok(self):
        gl_project = mock.Mock()
        gl_mergerequest = mock.Mock()
        gl_mergerequest.iid = 2
        gl_mergerequest.target_branch = 'os-build'
        gl_project.mergerequests.list.return_value = [gl_mergerequest]
        gl_project.mergerequests.get.return_value = gl_mergerequest
        gl_project.name = 'kernel-ark'
        mock_src = '/src/kernel-ark/'

        # Clean merges
        m_args = ['git', 'merge', '--quiet', '--no-edit',
                  f"{gl_project.name}/merge-requests/{gl_mergerequest.iid}"]
        self._add_run_result(m_args, 0)
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            ret = mergehook.check_for_merge_conflicts(gl_project, gl_mergerequest, mock_src)
            self.assertIn(m_args, self._mocked_calls)
            self.assertEqual(ret, [])

    @mock.patch('webhook.mergehook.create_worktree_timestamp', mock.Mock(return_value=True))
    @mock.patch('webhook.mergehook._git_reset', mock.Mock(return_value=True))
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_check_for_merge_conflicts_bad(self):
        gl_project = mock.Mock()
        gl_mergerequest = mock.Mock()
        gl_mergerequest.iid = 2
        gl_mergerequest.target_branch = 'os-build'
        gl_project.mergerequests.list.return_value = [gl_mergerequest]
        gl_project.mergerequests.get.return_value = gl_mergerequest
        gl_project.name = 'kernel-ark'
        mock_src = '/src/kernel-ark/'
        error_text = (f"MR !{gl_mergerequest.iid} cannot be merged to "
                      f"{gl_project.name}/{gl_mergerequest.target_branch}  \n")

        # Failed merge
        self._mocked_calls = []
        m_args = ['git', 'merge', '--quiet', '--no-edit',
                  f"{gl_project.name}/merge-requests/{gl_mergerequest.iid}"]
        self._add_run_result(m_args, 5, 'Catastrophic error!')
        with mock.patch('subprocess.run', autospec=True, side_effect=self._mock_run):
            ret = mergehook.check_for_merge_conflicts(gl_project, gl_mergerequest, mock_src)
            self.assertIn(m_args, self._mocked_calls)
            self.assertEqual(ret, [error_text, 'Catastrophic error!'])

    @mock.patch('shutil.rmtree', mock.Mock(return_value=True))
    @mock.patch('webhook.mergehook._git', mock.Mock(return_value=True))
    def test_clean_up_temp_merge_branch(self):
        worktree_dir = '/src/kernel-ark-temp-merge-branch/'
        merge_branch = 'temp-merge-branch'
        with self.assertLogs('cki.webhook.mergehook', level='DEBUG') as logs:
            mergehook.clean_up_temp_merge_branch('mocked', merge_branch, worktree_dir)
            self.assertIn(f'Removed worktree {worktree_dir} and deleted branch {merge_branch}',
                          logs.output[-1])

    @mock.patch('webhook.mergehook.handle_stale_worktree', mock.Mock(return_value=True))
    @mock.patch('webhook.mergehook.clean_up_temp_merge_branch', mock.Mock(return_value=True))
    @mock.patch('webhook.common.update_webhook_comment', mock.Mock(return_value=True))
    @mock.patch('cki_lib.misc.get_nested_key')
    @mock.patch('webhook.mergehook._git')
    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.mergehook.check_for_other_merge_conflicts')
    @mock.patch('webhook.mergehook.check_for_merge_conflicts')
    def test_process_merge_request(self, mock_conflicts, mock_other_conflicts,
                                   mock_add_label, mock_git, mock_key):
        mock_inst = mock.Mock()
        mock_proj = mock.Mock()
        mock_mr = mock.Mock(iid=66)
        mock_mr.target_branch = 'os-build'
        mock_gql = mock.Mock()
        mock_src = '/src/kernel-ark/'
        mock_conflicts.return_value = ['MR !66 cannot be merged to os-build', 'CONFLICTS']
        mock_other_conflicts.return_value = ['CONFLICTS']
        mr_dict = {'iid': '66',
                   'author': {'username': 'shadowman'},
                   'title': 'Test MR',
                   'targetBranch': 'os-build',
                   'project': {'fullPath': 'cki-project/kernel-ark'}
                   }
        mock_key.check_query_results.return_value = [mr_dict]
        mergehook.process_merge_request(mock_inst, mock_proj, mock_mr, mock_gql, mock_src)
        mock_add_label.assert_called_with(mock_proj, 66, [defs.MERGE_CONFLICT_LABEL])

        mock_conflicts.return_value = []
        mergehook.process_merge_request(mock_inst, mock_proj, mock_mr, mock_gql, mock_src)
        mock_add_label.assert_called_with(mock_proj, 66, [defs.MERGE_WARNING_LABEL])

        mock_other_conflicts.return_value = []
        mergehook.process_merge_request(mock_inst, mock_proj, mock_mr, mock_gql, mock_src)
        mock_add_label.assert_called_with(mock_proj, 66, [f'Merge::{defs.READY_SUFFIX}'])

    @mock.patch('webhook.common.mr_action_affects_commits', mock.Mock(return_value=True))
    @mock.patch('webhook.common.do_not_run_hook', mock.Mock(return_value=False))
    @mock.patch('webhook.mergehook._git')
    @mock.patch('webhook.common.get_mr')
    @mock.patch('webhook.mergehook.process_merge_request')
    def test_process_mr_event(self, mock_process_mr, get_mr, mock_git):
        mock_inst = mock.Mock()
        mock_inst.projects.get.return_value = 'mock_proj'
        get_mr.return_value = 'mock_mr'
        msg = mock.Mock()
        msg.payload = {'object_kind': 'merge_request',
                       'project': {'id': defs.ARK_PROJECT_ID, 'web_url': 'https://web.url/g/p',
                                   'path_with_namespace': 'g/p'},
                       'object_attributes': {'iid': 2, 'action': 'update'},
                       'changes': {'labels': {'previous': [{'title': 'Acks::NeedsReview'}],
                                              'current': [{'title': 'Acks::OK'}]}}
                       }
        mock_gql = mock.Mock()
        mock_src = '/src/kernel-ark/'
        mergehook.process_mr_event(mock_inst, msg, mock_gql, mock_src)
        mock_process_mr.assert_called_with(mock_inst, 'mock_proj', 'mock_mr', mock_gql, mock_src)
        get_mr.return_value = []
        mock_process_mr.call_count = 0
        mergehook.process_mr_event(mock_inst, msg, mock_gql, mock_src)
        mock_process_mr.assert_not_called()

    @mock.patch('webhook.mergehook._git')
    @mock.patch('webhook.common.get_mr')
    @mock.patch('webhook.mergehook.process_merge_request')
    def test_process_note_event(self, mock_process_mr, get_mr, mock_git):
        mock_inst = mock.Mock()
        mock_inst.projects.get.return_value = 'mock_proj'
        msg = mock.Mock()
        msg.payload = {'object_kind': 'note',
                       'project': {'id': 1,
                                   'web_url': 'https://web.url/g/p',
                                   'path_with_namespace': 'g/p'},
                       'object_attributes': {'note': 'some comment',
                                             'noteable_type': 'MergeRequest'},
                       'merge_request': {'iid': 2}}
        mock_gql = mock.Mock()
        mock_src = '/src/kernel-ark/'

        get_mr.return_value = []
        mergehook.process_note_event(mock_inst, msg, mock_gql, mock_src)
        mock_process_mr.assert_not_called()

        get_mr.return_value = 'mock_mr'
        mergehook.process_note_event(mock_inst, msg, mock_gql, mock_src)
        mock_process_mr.assert_not_called()

        msg.payload = {'object_kind': 'note',
                       'project': {'id': defs.ARK_PROJECT_ID,
                                   'web_url': 'https://web.url/g/p',
                                   'path_with_namespace': 'g/p'},
                       'object_attributes': {'note': 'request-merge-evaluation',
                                             'noteable_type': 'MergeRequest'},
                       'merge_request': {'iid': 2}}
        mergehook.process_note_event(mock_inst, msg, mock_gql, mock_src)
        mock_process_mr.assert_called_with(mock_inst, 'mock_proj', 'mock_mr', mock_gql, mock_src)
