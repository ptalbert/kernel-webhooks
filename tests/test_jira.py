"""Tests for the table module."""
from unittest import TestCase

from tests import fakes_jira
from tests import fakes_jira_mrs as fakes_mrs
from webhook import jissue
from webhook import jissue_tests
from webhook.defs import JPFX
from webhook.rh_metadata import Projects


class TestJIssue(TestCase):
    """Tests for the JIssue class."""

    # expected Issue values when no JIRA or MRs are set.
    empty_mr_equal = {'_mrs': [],
                      'alias': 'JIssue #0',
                      'failed_tests': [],
                      '_id': 0,
                      'id': 'BOGUS',       # This should be set by the tester,
                      'ji_cves': [],
                      'ji_depends_on': [],
                      'ji_fix_version': '',
                      'policy_check_ok': (None, 'Check not done: No JIRA Issue'),
                      'commits': [],
                      'parent_mr_commits': []}

    empty_mr_is = {'_ji': None,
                   'scope': jissue.MrScope.INVALID,
                   'internal': False,
                   'untagged': False,
                   'ji': None,
                   'mr': None,
                   'parent_mr': None,
                   'ji_branch': None,
                   'ji_is_verified': False,
                   'ji_project': None,
                   'ji_resolution': None,
                   'ji_status': jissue.JIStatus.UNKNOWN,
                   'cve_ids': None,
                   'is_cve_tracker': False,
                   'is_dependency': False,
                   'is_merged': False,
                   'in_mr_description': False,
                   'test_list': ['BOGUS']}  # This should be set by the tester.

    def validate_issue(self, jissue, assert_equal, assert_is):
        """Helper to validate a JIRA Issue object."""
        print(f'Testing JIssue {jissue}...')
        for attribute, value in assert_equal.items():
            print(f'{attribute} should equal: {value}')
            self.assertEqual(getattr(jissue, attribute), value)
        for attribute, value in assert_is.items():
            print(f'{attribute} should be: {value}')
            self.assertIs(getattr(jissue, attribute), value)

    def test_jissue_init_empty(self):
        """Creates a JIssue with no JIRA object set or MRs."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        # Expected values for an empty JIssue.
        assert_equal['id'] = 0
        assert_equal['_id'] = 0
        assert_is.pop('_id', None)
        assert_is['test_list'] = jissue_tests.ISSUE_TESTS

        test_jissue = jissue.JIssue()
        self.validate_issue(test_jissue, assert_equal, assert_is)

    def test_jissue_new_internal_no_mrs(self):
        """Creates a JIssue representing descriptions marked INTERNAL."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        # Unique values of an Internal JIRA Issue
        assert_equal['alias'] = 'JIssue #INTERNAL'
        assert_equal['id'] = jissue.INTERNAL_JISSUE
        assert_is['internal'] = True
        assert_is['test_list'] = jissue_tests.INTERNAL_TESTS

        test_jissue = jissue.JIssue.new_internal(mrs=[])
        self.validate_issue(test_jissue, assert_equal, assert_is)

    def test_jissue_new_missing_no_mrs(self):
        """Creates a JIssue representing a MISSING JI."""
        issue_id = f'{JPFX}1234567'
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        # Expected values for this Missing JIssue
        assert_equal['alias'] = f'JIssue #{JPFX}1234567'
        assert_equal['id'] = issue_id
        assert_equal['_id'] = issue_id
        assert_is.pop('_id', None)
        assert_is['test_list'] = jissue_tests.ISSUE_TESTS

        test_jissue = jissue.JIssue.new_missing(issue_id, mrs=[])
        self.validate_issue(test_jissue, assert_equal, assert_is)

    def test_jissue_new_missing_cve_no_mrs(self):
        """Creates a JIssue representing a MISSING CVE tracker JI."""
        issue_id = 'CVE-1998-12345'
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        # Expected values for this Missing CVE JIssue
        assert_equal['alias'] = 'CVE-1998-12345 (JIRA Issue missing)'
        assert_equal['policy_check_ok'] = (None, 'Check not done: CVE tracker issue')
        assert_equal['cve_ids'] = [issue_id]
        assert_equal['id'] = issue_id
        assert_equal['_id'] = issue_id
        assert_is.pop('_id', None)
        assert_is.pop('cve_ids', None)
        assert_is['is_cve_tracker'] = True
        assert_is['test_list'] = jissue_tests.CVE_TESTS

        test_jissue = jissue.JIssue.new_missing(issue_id, mrs=[])
        self.validate_issue(test_jissue, assert_equal, assert_is)

    def test_jissue_new_untagged_no_mrs(self):
        """Creates a JIssue representing descriptions with no tags (UNTAGGED)."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        # Unique values of an Untagged JIssue
        assert_equal['alias'] = 'JIssue #UNTAGGED'
        assert_equal['id'] = jissue.UNTAGGED_JISSUE
        assert_is['untagged'] = True
        assert_is['test_list'] = jissue_tests.UNTAGGED_TESTS

        test_jissue = jissue.JIssue.new_untagged(mrs=[])
        self.validate_issue(test_jissue, assert_equal, assert_is)

    def test_jissue_new_from_ji_no_mrs_ji1234567(self):
        """Creates a JIssue linked to the given JIRA Issue."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        assert_equal['alias'] = f'JIssue #{JPFX}1234567'
        assert_equal['id'] = f'{JPFX}1234567'
        assert_equal['ji_cves'] = ['CVE-1235-13516']
        assert_equal['ji_fix_version'] = '9.1.0'
        assert_equal['policy_check_ok'] = (None, 'Check not done: No MR or Branch data')
        assert_is['ji'] = fakes_jira.JI1234567
        assert_is['_ji'] = fakes_jira.JI1234567
        assert_is['ji_status'] = jissue.JIStatus.DEVELOPMENT
        assert_is['test_list'] = jissue_tests.ISSUE_TESTS

        test_jissue = jissue.JIssue.new_from_ji(ji=fakes_jira.JI1234567, mrs=[])
        self.validate_issue(test_jissue, assert_equal, assert_is)

    def test_jissue_new_from_ji_no_mrs_ji2323232(self):
        """Creates a JIssue linked to the given JIRA Issue."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        assert_equal['alias'] = f'JIssue #{JPFX}2323232'
        assert_equal['id'] = f'{JPFX}2323232'
        assert_equal['ji_fix_version'] = '9.1.0'
        assert_equal['policy_check_ok'] = (None, 'Check not done: No MR or Branch data')
        assert_is['ji'] = fakes_jira.JI2323232
        assert_is['_ji'] = fakes_jira.JI2323232
        assert_is['ji_status'] = jissue.JIStatus.READY_FOR_QA
        assert_is['test_list'] = jissue_tests.ISSUE_TESTS
        assert_is['ji_is_verified'] = True

        test_jissue = jissue.JIssue.new_from_ji(ji=fakes_jira.JI2323232, mrs=[])
        self.validate_issue(test_jissue, assert_equal, assert_is)

    def test_jissue_new_from_ji_no_mrs_ji7777777(self):
        """Creates a JIssue linked to the given JIRA issue."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        assert_equal['alias'] = f'JIssue #{JPFX}7777777'
        assert_equal['id'] = f'{JPFX}7777777'
        assert_equal['ji_fix_version'] = '9.1'
        assert_equal['policy_check_ok'] = (None, 'Check not done: No MR or Branch data')
        assert_is['ji'] = fakes_jira.JI7777777
        assert_is['_ji'] = fakes_jira.JI7777777
        assert_is['ji_status'] = jissue.JIStatus.DEVELOPMENT
        assert_is['test_list'] = jissue_tests.ISSUE_TESTS

        test_jissue = jissue.JIssue.new_from_ji(ji=fakes_jira.JI7777777, mrs=[])
        self.validate_issue(test_jissue, assert_equal, assert_is)

    def test_jissue_new_from_ji_no_mrs_ji2345678(self):
        """Creates a JIssue linked to the given JIRA Issue."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        assert_equal['alias'] = f'JIssue #{JPFX}2345678'
        assert_equal['id'] = f'{JPFX}2345678'
        assert_equal['ji_cves'] = ['CVE-2022-43210']
        assert_equal['ji_fix_version'] = '9.0'
        assert_equal['policy_check_ok'] = (None, 'Check not done: No MR or Branch data')
        assert_is['ji'] = fakes_jira.JI2345678
        assert_is['_ji'] = fakes_jira.JI2345678
        assert_is['ji_status'] = jissue.JIStatus.READY_FOR_QA
        assert_is['test_list'] = jissue_tests.ISSUE_TESTS
        assert_is['ji_is_verified'] = True

        test_jissue = jissue.JIssue.new_from_ji(ji=fakes_jira.JI2345678, mrs=[])
        self.validate_issue(test_jissue, assert_equal, assert_is)

    def test_jissue_new_from_ji_no_mrs_ji3456789(self):
        """Creates a JIssue linked to the given JIRA Issue."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        assert_equal['alias'] = f'CVE-1235-13516 (JIssue #{JPFX}3456789)'
        assert_equal['id'] = f'{JPFX}3456789'
        assert_equal['ji_cves'] = ['CVE-1235-13516']
        assert_equal['policy_check_ok'] = (None, 'Check not done: CVE tracker issue')
        assert_equal['cve_ids'] = ['CVE-1235-13516']
        assert_is.pop('cve_ids', None)
        assert_is['ji'] = fakes_jira.JI3456789
        assert_is['_ji'] = fakes_jira.JI3456789
        assert_is['ji_status'] = jissue.JIStatus.NEW
        assert_is['is_cve_tracker'] = True
        assert_is['test_list'] = jissue_tests.CVE_TESTS

        test_jissue = jissue.JIssue.new_from_ji(ji=fakes_jira.JI3456789, mrs=[])
        self.validate_issue(test_jissue, assert_equal, assert_is)

    def test_jissue_new_from_ji_no_mrs_ji4567890(self):
        """Creates a JIssue linked to the given JIRA Issue."""
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        assert_equal['alias'] = f'CVE-2022-7549, CVE-2022-7550 (JIssue #{JPFX}4567890)'
        assert_equal['id'] = f'{JPFX}4567890'
        assert_equal['ji_cves'] = ['CVE-2022-7549', 'CVE-2022-7550']
        assert_equal['policy_check_ok'] = (None, 'Check not done: CVE tracker issue')
        assert_equal['cve_ids'] = ['CVE-2022-7549', 'CVE-2022-7550']
        assert_is.pop('cve_ids', None)
        assert_is['ji'] = fakes_jira.JI4567890
        assert_is['_ji'] = fakes_jira.JI4567890
        assert_is['ji_resolution'] = jissue.JIResolution.ERRATA
        assert_is['ji_status'] = jissue.JIStatus.CLOSED
        assert_is['is_cve_tracker'] = True
        assert_is['test_list'] = jissue_tests.CVE_TESTS

        test_jissue = jissue.JIssue.new_from_ji(ji=fakes_jira.JI4567890, mrs=[])
        self.validate_issue(test_jissue, assert_equal, assert_is)

    def test_equality(self):
        """Returns True if the IDs match, otherwise False."""
        jissue1 = jissue.JIssue.new_from_ji(ji=fakes_jira.JI3456789, mrs=[])
        jissue2 = jissue.JIssue.new_from_ji(ji=fakes_jira.JI4567890, mrs=[])
        jissue3 = jissue.JIssue.new_missing(ji_id=f'{JPFX}3456789', mrs=[])

        self.assertNotEqual(jissue1, jissue2)
        self.assertEqual(jissue1, jissue3)

        # Returns False if the other is not a JIssue instance.
        self.assertFalse(jissue1 == 'hello')

    def test_id_setter(self):
        """Sets the id property if self.ji is not set and not internal/untagged."""
        # Setting id property raises ValueError if the ji property is set.
        test_jissue = jissue.JIssue.new_from_ji(ji=fakes_jira.JI2323232, mrs=[])
        with self.assertRaises(ValueError):
            test_jissue.id = f'{JPFX}1234567'

        # Setting the id property of a JIssue marked internal or untagged raises a ValueError
        test_jissue = jissue.JIssue.new_internal(mrs=[])
        with self.assertRaises(ValueError):
            test_jissue.id = f'{JPFX}7654321'
        test_jissue = jissue.JIssue.new_untagged(mrs=[])
        with self.assertRaises(ValueError):
            test_jissue.id = f'{JPFX}7654321'

        # Setting the id property works for a JIssue with no ji.
        test_jissue = jissue.JIssue.new_missing(f'{JPFX}1234567', mrs=[])
        self.assertEqual(test_jissue.id, f'{JPFX}1234567')
        test_jissue.id = f'{JPFX}7654321'
        self.assertEqual(test_jissue.id, f'{JPFX}7654321')

    def test_jissues_with_mrs(self):
        """Returns the expected values when MRs are present."""
        projects = Projects(yaml_path='tests/fake_rh_metadata.yaml')
        mr410 = fakes_mrs.make_mr('group/centos-stream-9', 410, projects=projects,
                                  query_results_list=[fakes_mrs.MR410_DICT, fakes_mrs.MR404_DICT])
        mr404 = fakes_mrs.make_mr('group/centos-stream-9', 404, projects=projects,
                                  query_results_list=[fakes_mrs.MR404_DICT], is_dependency=True)
        mr410.depends_mrs.append(mr404)
        mr410.description._depends.update({f'{JPFX}2323232'})
        mrs = [mr410, mr404]
        ji1234567 = jissue.JIssue.new_from_ji(ji=fakes_jira.JI1234567, mrs=mrs)
        ji2323232 = jissue.JIssue.new_from_ji(ji=fakes_jira.JI2323232, mrs=mrs)
        ji3456789 = jissue.JIssue.new_from_ji(ji=fakes_jira.JI3456789, mrs=mrs)

        # ji1234567 properties
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        assert_equal['alias'] = f'JIssue #{JPFX}1234567'
        assert_equal['id'] = f'{JPFX}1234567'
        assert_equal['ji_cves'] = ['CVE-1235-13516']
        assert_equal['ji_fix_version'] = '9.1.0'
        assert_equal['policy_check_ok'] = (None, 'Check not done: Branch has no policy')
        assert_equal['commits'] = ['0aa467549b4e997d023c29f4d481aee01b9e9471',
                                   'e53eab9f887f784044ad32ef5c082695831d90d9',
                                   '88cdd4035228dac16878eb907381afea6ceffeaa']
        assert_equal['parent_mr_commits'] = assert_equal['commits']
        assert_equal['_mrs'] = mrs
        assert_is['ji'] = fakes_jira.JI1234567
        assert_is['_ji'] = fakes_jira.JI1234567
        assert_is['ji_branch'] = mr410.projects.projects[11223344].branches[0]
        assert_is['ji_project'] = mr410.projects.projects[11223344]
        assert_is['ji_status'] = jissue.JIStatus.DEVELOPMENT
        assert_is['in_mr_description'] = True
        assert_is['mr'] = mr410
        assert_is['parent_mr'] = mr410
        assert_is['test_list'] = jissue_tests.ISSUE_TESTS

        self.validate_issue(ji1234567, assert_equal, assert_is)

        # ji2323232 properties
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        assert_equal['alias'] = f'JIssue #{JPFX}2323232'
        assert_equal['id'] = f'{JPFX}2323232'
        assert_equal['policy_check_ok'] = (None, 'Check not done: Branch has no policy')
        assert_equal['ji_fix_version'] = '9.1.0'
        assert_equal['commits'] = ['ce1fdd9354bdc315e49a40dc9da3ab03bf6af7b3',
                                   'f77278fcd9cef99358adc7f5e077be795a54ffca']
        assert_equal['parent_mr_commits'] = assert_equal['commits']
        assert_equal['_mrs'] = mrs
        assert_is['ji'] = fakes_jira.JI2323232
        assert_is['_ji'] = fakes_jira.JI2323232
        assert_is['ji_branch'] = mr410.projects.projects[11223344].branches[0]
        assert_is['ji_project'] = mr410.projects.projects[11223344]
        assert_is['ji_status'] = jissue.JIStatus.READY_FOR_QA
        assert_is['in_mr_description'] = True
        assert_is['is_dependency'] = True
        assert_is['mr'] = mr404
        assert_is['parent_mr'] = mr410
        assert_is['test_list'] = jissue_tests.DEP_TESTS
        assert_is['ji_is_verified'] = True

        self.validate_issue(ji2323232, assert_equal, assert_is)

        # ji3456789 properties
        assert_equal = self.empty_mr_equal.copy()
        assert_is = self.empty_mr_is.copy()

        assert_equal['alias'] = f'CVE-1235-13516 (JIssue #{JPFX}3456789)'
        assert_equal['id'] = f'{JPFX}3456789'
        assert_equal['ji_cves'] = ['CVE-1235-13516']
        assert_equal['policy_check_ok'] = (None, 'Check not done: CVE tracker issue')
        assert_equal['cve_ids'] = ['CVE-1235-13516']
        assert_equal['commits'] = ['0aa467549b4e997d023c29f4d481aee01b9e9471',
                                   'e53eab9f887f784044ad32ef5c082695831d90d9',
                                   '88cdd4035228dac16878eb907381afea6ceffeaa']
        assert_equal['parent_mr_commits'] = assert_equal['commits']
        assert_equal['_mrs'] = mrs
        assert_is.pop('cve_ids', None)
        assert_is['ji'] = fakes_jira.JI3456789
        assert_is['_ji'] = fakes_jira.JI3456789
        assert_is['ji_status'] = jissue.JIStatus.NEW
        assert_is['in_mr_description'] = True
        assert_is['is_cve_tracker'] = True
        assert_is['mr'] = mr410
        assert_is['parent_mr'] = mr410
        assert_is['test_list'] = jissue_tests.CVE_TESTS

        self.validate_issue(ji3456789, assert_equal, assert_is)

        mr410.depends_mrs.clear()
