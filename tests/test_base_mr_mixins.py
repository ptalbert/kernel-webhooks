"""Tests for the base_mr mixins."""
from dataclasses import dataclass
from dataclasses import field
from unittest import TestCase
from unittest import mock

import responses
from responses.matchers import json_params_matcher

from tests.test_approval_rules import ALL_MEMBERS_RULE
from tests.test_approval_rules import X86_RULE
from tests.test_owners import OWNERS_ENTRY_1
from tests.test_owners import OWNERS_ENTRY_2
from tests.test_owners import OWNERS_HEADER
from webhook import base_mr_mixins
from webhook.defs import GITFORGE
from webhook.graphql import GitlabGraph
from webhook.owners import Parser
from webhook.pipelines import PipelineResult
from webhook.users import UserCache

API_URL = f'{GITFORGE}/api/graphql'
NAMESPACE = 'group/project'
MR_ID = 123
MR_URL = f'{GITFORGE}/{NAMESPACE}/-/merge_requests/{MR_ID}'


class TestApprovalsMixin(TestCase):
    """Tests for the ApprovalsMixin."""

    @dataclass(repr=False)
    class BasicApprovals(base_mr_mixins.ApprovalsMixin, base_mr_mixins.GraphMixin):
        """A dummy class for testing the ApprovalsMixin."""
        user_cache: UserCache
        namespace: str = NAMESPACE
        iid: int = MR_ID

    @responses.activate
    def test_approvals_rules_property(self):
        """Returns a dict of the expected ApprovalRule objects."""
        graphql = GitlabGraph()
        user_cache = UserCache(graphql, NAMESPACE)
        match_query = base_mr_mixins.ApprovalsMixin.APPROVALS_QUERY.strip('\n')

        rules_list = [ALL_MEMBERS_RULE, X86_RULE]
        json_response = {'project': {'mr': {'approvalState': {'rules': rules_list}}}}
        responses.post(API_URL, json={'data': json_response},
                       match=[json_params_matcher({'query': match_query,
                                                   'variables': {'namespace': NAMESPACE,
                                                                 'mr_id': str(MR_ID)}},
                                                  strict_match=False)])

        basic_approvals = self.BasicApprovals(graphql, user_cache)
        found_rules = basic_approvals.approval_rules

        self.assertEqual(len(found_rules), 2)
        self.assertTrue('x86' in found_rules)
        self.assertTrue('All Members' in found_rules)


class TestPipelinesMixin(TestCase):
    """Tests for the PipelinessMixin."""

    @dataclass(repr=False)
    class BasicPipelines(base_mr_mixins.PipelinesMixin, base_mr_mixins.GraphMixin):
        """A dummy class for testing the PipelinesMixin."""
        user_cache: UserCache
        namespace: str = NAMESPACE
        iid: int = MR_ID

    @responses.activate
    def test_pipelines(self):
        """Returns a dict of the expected ApprovalRule objects."""
        graphql = GitlabGraph()
        user_cache = UserCache(graphql, NAMESPACE)
        match_query = base_mr_mixins.PipelinesMixin.PIPELINES_QUERY.strip('\n')

        ds_pipe = {'id': 'gid://Gitlab/pipeline/456', 'status': 'SUCCESS'}
        job_nodes = [{'id': 123, 'name': 'c9s_merge_request', 'downstreamPipeline': ds_pipe}]
        json_response = {'project': {'mr': {'headPipeline': {'jobs': {'nodes': job_nodes}}}}}
        rsp = responses.post(API_URL, json={'data': json_response},
                             match=[json_params_matcher({'query': match_query,
                                                         'variables': {'namespace': NAMESPACE,
                                                                       'mr_id': str(MR_ID)}},
                                                        strict_match=False)])

        basic_pipelines = self.BasicPipelines(graphql, user_cache)
        found_pipes1 = basic_pipelines.pipelines
        self.assertEqual(len(found_pipes1), 1)
        self.assertTrue(isinstance(found_pipes1[0], PipelineResult))
        self.assertEqual(rsp.call_count, 1)
        # Confirm the result is cached.
        found_pipes2 = basic_pipelines.pipelines
        self.assertEqual(rsp.call_count, 1)
        self.assertIs(found_pipes1, found_pipes2)
        # Confirm fresh_pipelines is the same but not the cached value.
        found_pipes3 = basic_pipelines.fresh_pipelines
        self.assertEqual(rsp.call_count, 2)
        self.assertIsNot(found_pipes3, found_pipes2)
        self.assertEqual(found_pipes3, found_pipes2)
        self.assertEqual(len(found_pipes3), 1)
        self.assertTrue(isinstance(found_pipes3[0], PipelineResult))


class TestOwnersMixin(TestCase):
    """Tests for the OwnersMixin."""

    RH_YAML = (" - subsystem: RH Subsystem\n"
               "   labels:\n"
               "     name: redhat\n"
               "     readyForMergeDeps:\n"
               "       - testDep\n"
               "   status: Maintained\n"
               "   requiredApproval: false\n"
               "   maintainers:\n"
               "     - name: User 10\n"
               "       email: user10@redhat.com\n"
               "     - name: User 20\n"
               "       email: user20@redhat.com\n"
               "   reviewers:\n"
               "     - name: User 30\n"
               "       email: user30@redhat.com\n"
               "       restricted: true\n"
               "     - name: User 40\n"
               "       email: user40@redhat.com\n"
               "   paths:\n"
               "       includes:\n"
               "          - makefile\n"
               "          - redhat/\n")

    @dataclass(repr=False)
    class BasicOwners(base_mr_mixins.OwnersMixin):
        """A dummy class for testing the OwnerssMixin."""
        files: list = field(default_factory=list)

    @mock.patch('webhook.base_mr_mixins.get_owners_parser')
    def test_owners_mixin_unmerged(self, mock_get_owners):
        """Provides expected values for the given files list."""
        mock_get_owners.return_value = Parser(OWNERS_HEADER + self.RH_YAML + OWNERS_ENTRY_1 +
                                              OWNERS_ENTRY_2)

        params = {'owners_path': 'owners.yaml',
                  'kernel_src': '',
                  'files': ['redhat/Makefile', 'redhat/configs/rhel/generic/CONFIG_ITEM']}
        basic_owners = self.BasicOwners(**params)
        self.assertEqual(basic_owners.all_files, set(basic_owners.files))
        self.assertEqual(basic_owners.config_items, ['CONFIG_ITEM'])
        self.assertIs(basic_owners.merge_entries, False)
        self.assertEqual(basic_owners.owners, mock_get_owners.return_value)
        self.assertEqual(len(basic_owners.owners_entries), 2)

    @mock.patch('webhook.base_mr_mixins.get_owners_parser')
    def test_owners_mixin_merged(self, mock_get_owners):
        """Provides expected values for the given files list."""
        parser = Parser(OWNERS_HEADER + self.RH_YAML + OWNERS_ENTRY_1 + OWNERS_ENTRY_2)
        # delete the paths excludes so we match config items
        del parser._entries[2]._entry['paths']['excludes']
        mock_get_owners.return_value = parser

        params = {'owners_path': 'owners.yaml',
                  'kernel_src': '',
                  'files': ['redhat/Makefile', 'redhat/configs/rhel/generic/CONFIG_ITEM'],
                  'merge_entries': True}
        basic_owners = self.BasicOwners(**params)
        self.assertEqual(basic_owners.all_files, set(basic_owners.files))
        self.assertEqual(basic_owners.config_items, ['CONFIG_ITEM'])
        self.assertIs(basic_owners.merge_entries, True)
        self.assertEqual(basic_owners.owners, mock_get_owners.return_value)
        self.assertEqual(len(basic_owners.owners_entries), 1)
        entry = basic_owners.owners_entries[0]
        self.assertEqual(len(entry.maintainers), 4)
        self.assertEqual(len(entry.reviewers), 5)
        self.assertIs(entry.required_approvals, True)
