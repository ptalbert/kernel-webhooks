"""Webhook interaction tests."""
from copy import deepcopy
from unittest import TestCase
from unittest import mock

import responses
from responses.matchers import json_params_matcher

from tests import fake_payloads
from tests import fakes
from tests.test_owners import OWNERS_ENTRY_1
from tests.test_owners import OWNERS_ENTRY_2
from tests.test_owners import OWNERS_HEADER
from webhook import defs
from webhook import subsystems
from webhook.graphql import GitlabGraph
from webhook.owners import Parser
from webhook.rh_metadata import Projects

API_URL = f'{defs.GITFORGE}/api/graphql'

# Expected values for a "bare" SubsysMR.
DEFAULT_SUBSYS_ATTRS = {'current_ss_names': set(),
                        'expected_ss_names': set(),
                        'stale_ss_names': set(),
                        'expected_ss_labels': [],
                        'current_ci_labels': set(),
                        'current_ci_names': set(),
                        'expected_ci_names': set(),
                        'missing_ci_labels': set(),
                        'stale_ci_labels': set(),
                        'current_ci_scope': None,
                        'expected_ci_scope': defs.MrScope.OK,
                        'expected_ci_labels': []}


class TestSubsysMR(TestCase):
    """Tests for the SubsysMR dataclass."""

    @classmethod
    def setUpClass(cls):
        """Confirms the DEFAULT_SUBSYS_ATTRS dict keys match the module properties."""
        subsys_attributes = {key for key in subsystems.SubsysMR.__dict__ if not key.startswith('_')}
        dict_keys = set(DEFAULT_SUBSYS_ATTRS.keys())
        if subsys_attributes != dict_keys:
            err = 'DEFAULT_SUBSYS_ATTRS keys do not match SubsysMR attributes'
            raise AttributeError(f'{err}: {subsys_attributes}')

    @responses.activate
    @mock.patch('webhook.base_mr_mixins.get_owners_parser')
    def _test_subsys_mr(self, mock_get_parser, response, parser_entries, expected_values):
        """Create and test a SubsysMR."""
        # Set up the query response
        mr_id = fake_payloads.MR_IID
        namespace = fake_payloads.PROJECT_PATH_WITH_NAMESPACE
        responses.post(API_URL, json={'data': response},
                       match=[json_params_matcher({'variables': {'mr_id': str(mr_id),
                                                                 'namespace': namespace}},
                                                  strict_match=False)])

        # Set up the owners Parser data.
        mock_get_parser.return_value = Parser(OWNERS_HEADER + parser_entries)

        # Set up the expected SubsysMR attribute values.
        subsys_attrs = deepcopy(DEFAULT_SUBSYS_ATTRS)
        if expected_values:
            # Don't accept anything that isn't already there.
            if key_diff := set(expected_values.keys()).difference(subsys_attrs):
                raise KeyError(f'expected_values has keys not in DEFAULT_SUBSYS_ATTRS: {key_diff}')
            subsys_attrs.update(expected_values)

        # Set up and create the SubsysMR.
        with mock.patch('webhook.rh_metadata.load') as mock_load:
            mock_load.return_value = deepcopy(fake_payloads.RH_METADATA)
            projects = Projects()
        subsys_mr = subsystems.SubsysMR(GitlabGraph(), fakes.FakeGitLab(), projects,
                                        fake_payloads.MR_URL)

        # Do the tests.
        for key, value in subsys_attrs.items():
            with self.subTest(key=key, value=value):
                test = self.assertCountEqual if isinstance(value, list) else self.assertEqual
                test(getattr(subsys_mr, key), value)

    def test_no_ci_or_ss(self):
        """Returns all default values due to no matching files."""
        response = deepcopy(fake_payloads.BASE_MR_RESPONSE)
        response['project']['mr']['files'] = []
        self._test_subsys_mr(response=response, parser_entries=OWNERS_ENTRY_1, expected_values=None)

    def test_base_mr_response(self):
        """Returns a SubsysMR with expected attributes for the BASE_MR_RESPONSE."""
        response = deepcopy(fake_payloads.BASE_MR_RESPONSE)
        entries = OWNERS_ENTRY_1 + OWNERS_ENTRY_2
        values = {'expected_ss_names': {'redhat'},
                  'expected_ci_names': {'testDep'},
                  'missing_ci_labels': {'ExternalCI::testDep::NeedsTesting'},
                  'expected_ss_labels': ['Subsystem:redhat'],
                  'expected_ci_labels': ['ExternalCI::testDep::NeedsTesting'],
                  'expected_ci_scope': defs.MrScope.READY_FOR_QA}
        self._test_subsys_mr(response=response, parser_entries=entries, expected_values=values)

    def test_draft_response(self):
        """Returns a SubsysMR without any ExpectedCI labels since this is a Draft."""
        mr_labels = ['ExternalCI::testDep::NeedsTesting',
                     'ExternalCI::old_test::Waived',
                     'ExternalCI::NeedsTesting']
        response = deepcopy(fake_payloads.BASE_MR_RESPONSE)
        response['project']['mr']['draft'] = True
        response['project']['mr']['labels']['nodes'] = [{'title': name} for name in mr_labels]
        entries = OWNERS_ENTRY_1 + OWNERS_ENTRY_2
        values = {'current_ci_labels': {'ExternalCI::testDep::NeedsTesting',
                                        'ExternalCI::old_test::Waived'},
                  'current_ci_names': {'testDep', 'old_test'},
                  'stale_ci_labels': {'ExternalCI::testDep::NeedsTesting',
                                      'ExternalCI::old_test::Waived'},
                  'expected_ss_names': {'redhat'},
                  'expected_ss_labels': ['Subsystem:redhat'],
                  'current_ci_scope': defs.MrScope.NEEDS_TESTING,
                  'expected_ci_scope': defs.MrScope.OK}
        self._test_subsys_mr(response=response, parser_entries=entries, expected_values=values)

    def test_custom_response(self):
        """Returns a SubsysMR with expected attributes set."""
        mr_files = ['redhat/Makefile',
                    'includes/net/ipv8.c',
                    'drivers/scsi/scsi.h']
        mr_labels = ['ExternalCI::x86_testing::NeedsTesting',
                     'ExternalCI::scsi_testing::OK',
                     'ExternalCI::NeedsTesting',
                     'Subsystem:scsi',
                     'Subsystem:x86']
        response = deepcopy(fake_payloads.BASE_MR_RESPONSE)
        response['project']['mr']['files'] = [{'path': name} for name in mr_files]
        response['project']['mr']['labels']['nodes'] = [{'title': name} for name in mr_labels]

        entry1 = (' - subsystem: Net\n'
                  '   labels:\n'
                  '    name: net\n'
                  '    readyForMergeDeps:\n'
                  '     - net_testing\n'
                  '   paths:\n'
                  '    includes:\n'
                  '     - includes/net/\n')
        entry2 = (' - subsystem: SCSI\n'
                  '   labels:\n'
                  '    name: scsi\n'
                  '    readyForMergeDeps:\n'
                  '     - scsi_testing\n'
                  '     - hdd_tests\n'
                  '   paths:\n'
                  '    includes:\n'
                  '     - drivers/scsi/\n')
        entry3 = (' - subsystem: redhat\n'
                  '   labels:\n'
                  '    name: redhat\n'
                  '   paths:\n'
                  '    includes:\n'
                  '     - redhat/\n')
        entries = entry1 + entry2 + entry3
        values = {'current_ss_names': {'scsi', 'x86'},
                  'expected_ss_names': {'net', 'scsi', 'redhat'},
                  'stale_ss_names': {'x86'},
                  'expected_ss_labels': ['Subsystem:net', 'Subsystem:scsi', 'Subsystem:redhat'],
                  'expected_ci_labels': ['ExternalCI::net_testing::NeedsTesting',
                                         'ExternalCI::scsi_testing::OK',
                                         'ExternalCI::hdd_tests::NeedsTesting'],
                  'missing_ci_labels': {'ExternalCI::net_testing::NeedsTesting',
                                        'ExternalCI::hdd_tests::NeedsTesting'},
                  'stale_ci_labels': {'ExternalCI::x86_testing::NeedsTesting'},
                  'expected_ci_names': {'net_testing', 'scsi_testing', 'hdd_tests'},
                  'current_ci_labels': {'ExternalCI::x86_testing::NeedsTesting',
                                        'ExternalCI::scsi_testing::OK'},
                  'current_ci_names': {'x86_testing', 'scsi_testing'},
                  'current_ci_scope': defs.MrScope.NEEDS_TESTING,
                  'expected_ci_scope': defs.MrScope.NEEDS_TESTING}
        self._test_subsys_mr(response=response, parser_entries=entries, expected_values=values)


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestHelpers(TestCase):
    """Tests for the subsystems functions."""

    OWNERS_YAML = ("subsystems:\n"
                   " - subsystem: MEMORY MANAGEMENT\n"
                   "   labels:\n"
                   "     name: mm\n"
                   "   paths:\n"
                   "       includes:\n"
                   "          - include/linux/mm.h\n"
                   "          - include/linux/vmalloc.h\n"
                   "          - mm/\n"
                   " - subsystem: NETWORKING\n"
                   "   labels:\n"
                   "     name: net\n"
                   "     readyForMergeDeps:\n"
                   "       - lnst\n"
                   "   paths:\n"
                   "       includes:\n"
                   "          - include/linux/net.h\n"
                   "          - include/net/\n"
                   "          - net/\n"
                   " - subsystem: XDP\n"
                   "   labels:\n"
                   "     name: xdp\n"
                   "   paths:\n"
                   "       includes:\n"
                   "          - kernel/bpf/devmap.c\n"
                   "          - include/net/page_pool.h\n"
                   "       includeRegexes:\n"
                   "          - xdp\n")

    def test_user_wants_notification(self):
        user_data = {'all': ['net/'],
                     '8.y': ['include/net/bonding.h'],
                     '8.2': ['include/net/*'],
                     '8.1': ['*/net/*']}

        path_list = ['net/core/dev.c']
        self.assertTrue(subsystems.user_wants_notification(user_data, path_list, '8.y'))
        self.assertTrue(subsystems.user_wants_notification(user_data, path_list, '8.2'))
        self.assertTrue(subsystems.user_wants_notification(user_data, path_list, '8.1'))

        path_list = ['networking/ipv8.c']
        self.assertFalse(subsystems.user_wants_notification(user_data, path_list, '8.y'))
        self.assertFalse(subsystems.user_wants_notification(user_data, path_list, '8.2'))
        self.assertFalse(subsystems.user_wants_notification(user_data, path_list, '8.1'))

        path_list = ['include/net/bond_3ad.h']
        self.assertFalse(subsystems.user_wants_notification(user_data, path_list, '8.y'))
        self.assertTrue(subsystems.user_wants_notification(user_data, path_list, '8.2'))
        self.assertTrue(subsystems.user_wants_notification(user_data, path_list, '8.1'))

        path_list = ['include/net/bonding.h']
        self.assertTrue(subsystems.user_wants_notification(user_data, path_list, '8.y'))
        self.assertTrue(subsystems.user_wants_notification(user_data, path_list, '8.2'))
        self.assertTrue(subsystems.user_wants_notification(user_data, path_list, '8.1'))

        path_list = ['include/net/bonding/bonding.h']
        self.assertFalse(subsystems.user_wants_notification(user_data, path_list, '8.y'))
        self.assertFalse(subsystems.user_wants_notification(user_data, path_list, '8.2'))
        self.assertFalse(subsystems.user_wants_notification(user_data, path_list, '8.1'))

        path_list = ['include/net/bonding/bonding.h']
        self.assertFalse(subsystems.user_wants_notification(user_data, path_list, '8.y'))
        self.assertFalse(subsystems.user_wants_notification(user_data, path_list, '8.2'))
        self.assertFalse(subsystems.user_wants_notification(user_data, path_list, '8.1'))

    @mock.patch('os.listdir')
    @mock.patch('webhook.common.load_yaml_data')
    @mock.patch('webhook.subsystems.user_wants_notification')
    def test_do_usermapping(self, mock_user_wants, mock_loader, mock_listdir):
        # Cannot get file listing.
        with self.assertLogs('cki.webhook.subsystems', level='ERROR') as logs:
            mock_listdir.side_effect = PermissionError
            result = subsystems.do_usermapping('8.y', [], '/repo')
            self.assertEqual(result, [])
            self.assertIn('Problem listing path', logs.output[-1])
            mock_user_wants.assert_not_called()

        # Error loading user data.
        with self.assertLogs('cki.webhook.subsystems', level='ERROR') as logs:
            mock_listdir.side_effect = None
            mock_listdir.return_value = ['user1']
            mock_loader.side_effect = [None]
            result = subsystems.do_usermapping('8.y', [], '/repo')
            self.assertEqual(result, [])
            self.assertIn("Error loading user data from path '/repo/users/user1'.",
                          logs.output[-1])
            mock_user_wants.assert_not_called()

        mock_listdir.return_value = ['user1', 'user2', 'user3']
        mock_loader.side_effect = None
        path_list = ['include/net/bonding.h', 'net/core/dev.c']

        user1_data = {'all': ['include/net/bonding.h']}
        user2_data = {'all': ['drivers/net/bonding/']}
        user3_data = {'all': ['include/net/bonding.h']}
        mock_loader.side_effect = [user1_data, user2_data, user3_data]
        mock_user_wants.side_effect = [True, False, True]
        result = subsystems.do_usermapping('8.y', path_list, '/repo')
        call_list = [mock.call(user1_data, path_list, '8.y'),
                     mock.call(user2_data, path_list, '8.y'),
                     mock.call(user3_data, path_list, '8.y')]

        self.assertEqual(mock_user_wants.call_count, 3)
        mock_user_wants.assert_has_calls(call_list, any_order=True)
        self.assertEqual(sorted(result), ['user1', 'user3'])

    @mock.patch('webhook.common.update_webhook_comment')
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @mock.patch.dict('os.environ', {'KERNEL_WATCH_URL': 'https://gitlab.com/project1'})
    def test_post_notifications(self, add_comment):
        mock_inst = mock.Mock()
        mock_inst.user.username = "shadowman"
        mock_mr = mock.Mock()
        mock_mr.iid = 2
        mock_mr.participants.return_value = [{'username': 'user1'},
                                             {'username': 'user2'},
                                             {'username': 'user3'}]

        # No one new to notify.
        with self.assertLogs('cki.webhook.subsystems', level='INFO') as logs:
            user_list = ['user1', 'user3']
            subsystems.post_notifications(mock_inst, mock_mr, user_list)
            self.assertIn('No one new to notify.', logs.output[-1])
            mock_mr.participants.assert_called()
            add_comment.assert_not_called()

        # Create a note.
        with self.assertLogs('cki.webhook.subsystems', level='INFO') as logs:
            user_list = ['user1', 'user2', 'user4', 'user5']
            template = defs.NOTIFICATION_TEMPLATE
            note_text = template.format(header=defs.NOTIFICATION_HEADER,
                                        users='@user4 @user5',
                                        project=f'{defs.GITFORGE}/project1')
            subsystems.post_notifications(mock_inst, mock_mr, user_list)
            self.assertIn('Posting notification on MR 2:', logs.output[-1])
            mock_mr.participants.assert_called()
            add_comment.assert_called_once()
            add_comment.assert_called_with(mock_mr, "shadowman",
                                           defs.NOTIFICATION_HEADER, note_text)

    def test_update_mr_non_draft(self):
        """Updates the MR with the labels computed by the SubsysMR."""
        mr_attrs = {'url': 'https://gitlab.com/group/project/-/merge_request/123',
                    'all_files': {'Makefile'},
                    'draft': False,
                    'labels': [defs.Label('Subsystem:redhat'),
                               defs.Label('ExternalCI::abc::OK')],
                    'owners_entries': [],
                    'expected_ss_names': ['net', 'x86', 'cpu'],
                    'stale_ss_names': ['redhat'],
                    'stale_ci_labels': {defs.Label('ExternalCI::abc::OK')},
                    'current_ci_labels': [defs.Label('ExternalCI::abc::OK')],
                    'expected_ss_labels': [defs.Label('Subsystem:net'), defs.Label('Subsystem:x86'),
                                           defs.Label('Subsystem:cpu')],
                    'expected_ci_labels': [defs.Label('ExternalCI::team1::NeedsTesting')],
                    'current_ci_scope': defs.MrScope.OK,
                    'expected_ci_scope': defs.MrScope.NEEDS_TESTING,
                    'add_labels': mock.Mock(),
                    'remove_labels': mock.Mock()
                    }

        subsys_mr = mock.Mock(spec=list(mr_attrs.keys()), **mr_attrs)

        subsystems.update_mr(subsys_mr)
        remove_labels_calls = [mock.call(['Subsystem:redhat']),
                               mock.call(['ExternalCI::abc::OK'])]
        self.assertEqual(subsys_mr.remove_labels.mock_calls, remove_labels_calls)
        subsys_mr.add_labels.assert_called_once_with({'ExternalCI::NeedsTesting',
                                                      'ExternalCI::team1::NeedsTesting',
                                                      'Subsystem:x86',
                                                      'Subsystem:net',
                                                      'Subsystem:cpu'},
                                                     remove_scoped=False)

    def test_update_mr_draft(self):
        """Updates the MR with the labels computed by the SubsysMR."""
        mr_attrs = {'url': 'https://gitlab.com/group/project/-/merge_request/123',
                    'all_files': {'Makefile'},
                    'draft': True,
                    'labels': [defs.Label('Subsystem:redhat'), defs.Label('ExternalCI::Waived')],
                    'owners_entries': [],
                    'expected_ss_names': ['net', 'x86', 'cpu'],
                    'stale_ss_names': ['redhat'],
                    'stale_ci_labels': set(),
                    'current_ci_labels': [],
                    'expected_ss_labels': [defs.Label('Subsystem:net'), defs.Label('Subsystem:x86'),
                                           defs.Label('Subsystem:cpu')],
                    'expected_ci_labels': [],
                    'current_ci_scope': defs.MrScope.WAIVED,
                    'expected_ci_scope': defs.MrScope.OK,
                    'add_labels': mock.Mock(),
                    'remove_labels': mock.Mock()
                    }
        subsys_mr = mock.Mock(spec=list(mr_attrs.keys()), **mr_attrs)

        subsystems.update_mr(subsys_mr)
        remove_labels_calls = [mock.call(['Subsystem:redhat']),
                               mock.call(['ExternalCI::Waived'])]
        self.assertEqual(subsys_mr.remove_labels.mock_calls, remove_labels_calls)
        subsys_mr.add_labels.assert_called_once_with({'Subsystem:x86',
                                                      'Subsystem:net',
                                                      'Subsystem:cpu'},
                                                     remove_scoped=False)

    @mock.patch('webhook.subsystems.notify_users')
    @mock.patch('webhook.subsystems.update_mr')
    @mock.patch('webhook.subsystems.SubsysMR', mock.Mock())
    def test_process_mr_event(self, mock_update_mr, mock_notify_users):
        """Checks the event and updates the MR/notifies users as needed."""
        mock_instance = fakes.FakeGitLab()
        mock_graphql = mock.Mock()
        mock_projects = mock.Mock()
        payload = deepcopy(fake_payloads.MR_PAYLOAD)
        msg = mock.Mock(payload=payload)

        kwargs = {'local_repo_path': 'local_repo_path',
                  'owners_path': 'owners_path',
                  'linus_src': 'linus_src'}

        # No relevant changes, nothing to do.
        subsystems.process_mr_event(mock_instance, msg, mock_graphql, mock_projects)
        mock_update_mr.assert_not_called()
        mock_notify_users.assert_not_called()

        # Out of draft so update the MR.
        changes = {'title': {'previous': 'Draft: ' + payload['object_attributes']['title'],
                             'current': payload['object_attributes']['title']}}
        payload['changes'] = changes
        subsystems.process_mr_event(mock_instance, msg, mock_graphql, mock_projects, **kwargs)
        mock_update_mr.assert_called_once()
        mock_notify_users.assert_called_once()
