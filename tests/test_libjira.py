"""Tests of the libjira library."""
from copy import deepcopy
from unittest import TestCase
from unittest import mock

from jira.exceptions import JIRAError

from tests import fakes_jira
from webhook import libjira
from webhook.defs import GITFORGE
from webhook.defs import JIStatus
from webhook.defs import JPFX


class TestFetchJIssues(TestCase):
    """Tests for fetch_issues and its helpers."""

    fetch_issues_cache = libjira.fetch_issues.__defaults__[1]

    def set_fetch_issues_cache(self, issue_cache):
        """Terrible helper to set up the fetch_issues issue_cache."""
        self.fetch_issues_cache.clear()
        self.fetch_issues_cache.update(issue_cache)

    def test_clear_issue_cache(self):
        """Clears the input list."""
        fake_cache = ['abc', 123, fakes_jira.JI7777777]
        libjira._clear_issue_cache(fake_cache)
        self.assertEqual(len(fake_cache), 0)

    def test_getissues(self):
        """Returns a list of JIRA Issue objects."""
        mock_jiracon = mock.Mock()
        issue_list = [123, 456, 789]
        result = libjira._getissues(mock_jiracon, issue_list)
        self.assertNotEqual(result, None)
        mock_jiracon.search_issues.assert_called_with("key=123 or key=456 or key=789",
                                                      fields=libjira.JiraField.all())
        issue_list = ['RHEL-1']
        mock_jiracon.search_issues.side_effect = \
            JIRAError("An issue with key 'RHEL-1' does not exist for field 'key'",
                      errors=["This issue key does not exist"])
        with self.assertLogs('cki.webhook.libjira', level='WARNING') as log:
            result = libjira._getissues(mock_jiracon, issue_list)
            self.assertIn(f'User specified invalid jira issue(s): {issue_list}',
                          log.output[-1])
            self.assertEqual(result, [])

    def test_get_issues_by_id(self):
        """Returns the JIRA Issue objects from the cache whose id is in the jissue_ids list."""
        issue_cache = {f'{JPFX}7777777': fakes_jira.JI7777777,
                       f'{JPFX}2345678': fakes_jira.JI2345678,
                       f'{JPFX}2673283': fakes_jira.JI2673283}
        issue_ids = [f'{JPFX}7777777', f'{JPFX}2345678']
        self.assertEqual(libjira._get_issues_by_id(issue_ids, issue_cache),
                         {fakes_jira.JI7777777, fakes_jira.JI2345678})

    def test_get_missing_issues(self):
        """Returns the entries from the input issue_names which are not in the issue_cache."""
        issue_cache_list = {f'{JPFX}7777777': fakes_jira.JI7777777,
                            f'{JPFX}2345678': fakes_jira.JI2345678,
                            f'{JPFX}3456789': fakes_jira.JI3456789}.values()
        issue_names = [f'{JPFX}7777777', f'{JPFX}2233456',
                       'CVE-1235-13516', f'{JPFX}2345678', 'CVE-1921-16127']
        self.assertEqual(libjira.get_missing_issues(issue_names, issue_cache_list),
                         {f'{JPFX}2233456', 'CVE-1921-16127'})

    def test_fetch_issues_clear_and_empty(self):
        """Clears the cache before proceeding, no input issue_list."""
        issue_cache = {f'{JPFX}7777777': fakes_jira.JI7777777,
                       f'{JPFX}2345678': fakes_jira.JI2345678,
                       f'{JPFX}2673283': fakes_jira.JI2673283}
        self.set_fetch_issues_cache(issue_cache)

        self.assertEqual(libjira.fetch_issues([], clear_cache=True), set())
        self.assertEqual(len(self.fetch_issues_cache), 0)

    def test_fetch_issues_no_input(self):
        """Returns an empty list because the input issue_list is empty."""
        self.set_fetch_issues_cache([])
        self.assertEqual(libjira.fetch_issues([]), set())

    @mock.patch('webhook.libjira._getissues')
    def test_fetch_issues_all_new(self, mock_getissues):
        """Clears the cache before proceeding, no input issue_list."""
        self.set_fetch_issues_cache([])
        mock_getissues.return_value = [fakes_jira.JI7777777, fakes_jira.JI2345678,
                                       fakes_jira.JI3456789]
        ji_ids = [f'{JPFX}7777777', f'{JPFX}2345678', 'CVE-1235-13516']

        result = libjira.fetch_issues(ji_ids)
        self.assertCountEqual(result, set(mock_getissues.return_value))

    @mock.patch('webhook.libjira.connect_jira', return_value=mock.Mock())
    @mock.patch('webhook.libjira._getissues')
    def test_fetch_issues_with_cache(self, mock_getissues, connect):
        """Returns some results from the cache."""
        self.set_fetch_issues_cache({f'{JPFX}7777777': fakes_jira.JI7777777,
                                     f'{JPFX}3456789': fakes_jira.JI3456789})
        mock_getissues.return_value = [fakes_jira.JI2345678]
        ji_ids = [f'{JPFX}7777777', f'{JPFX}2345678', 'CVE-1235-13516']

        result = libjira.fetch_issues(ji_ids)
        mock_getissues.assert_called_with(connect.return_value, {f'{JPFX}2345678'})
        self.assertCountEqual(result, {fakes_jira.JI7777777, fakes_jira.JI2345678,
                                       fakes_jira.JI3456789})

    @mock.patch('webhook.libjira.connect_jira', return_value=mock.Mock())
    @mock.patch('webhook.libjira._getissues')
    def test_fetch_issues_missing(self, mock_getissues, connect):
        """Does not return all the expected data."""
        self.set_fetch_issues_cache({f'{JPFX}7777777': fakes_jira.JI7777777,
                                     f'{JPFX}3456789': fakes_jira.JI3456789})
        mock_getissues.return_value = [fakes_jira.JI2673283]
        ji_ids = [f'{JPFX}7777777', f'{JPFX}2673283', f'{JPFX}6436237', 'CVE-1235-13516']

        with self.assertLogs('cki.webhook.libjira', level='WARNING') as log:
            result = libjira.fetch_issues(ji_ids)
            self.assertIn('JIRA did not return data for these issues: {\'' + JPFX + '6436237\'}',
                          log.output[-1])
        mock_getissues.assert_called_with(connect.return_value,
                                          {f'{JPFX}2673283', f'{JPFX}6436237'})
        self.assertCountEqual(result, {fakes_jira.JI7777777, fakes_jira.JI3456789,
                                       fakes_jira.JI2673283})


class TestHelpers(TestCase):
    """Tests for helper functions."""
    JIRA01 = fakes_jira.FakeJI(id=1, fields=mock.Mock(status='UNKNOWN'))
    JIRA02 = fakes_jira.FakeJI(id=2, fields=mock.Mock(status='NEW'))
    JIRA03 = fakes_jira.FakeJI(id=4, fields=mock.Mock(status='DEVELOPMENT'))
    JIRA04 = fakes_jira.FakeJI(id=5, fields=mock.Mock(status='READY_FOR_QA'))
    JIRA05 = fakes_jira.FakeJI(id=6, fields=mock.Mock(status='TESTED'))
    JIRA06 = fakes_jira.FakeJI(id=7, fields=mock.Mock(status='CLOSED'))

    def test_issues_with_lower_status(self):
        """Returns the JIRA Issues whose status is lower than status and at least min_status."""

        issue_list = [self.JIRA01, self.JIRA02, self.JIRA03, self.JIRA04,
                      self.JIRA05, self.JIRA06]
        self.assertCountEqual(libjira.issues_with_lower_status(issue_list, JIStatus.DEVELOPMENT),
                              [self.JIRA02])
        self.assertCountEqual(libjira.issues_with_lower_status(issue_list, JIStatus.TESTED),
                              [self.JIRA02, self.JIRA03, self.JIRA04])
        self.assertCountEqual(libjira.issues_with_lower_status(issue_list, JIStatus.CLOSED,
                              JIStatus.READY_FOR_QA), [self.JIRA04, self.JIRA05])

    def test_update_issue_status_no_prod(self):
        """Moves qualifying issues to the given status and returns them in a list."""
        # Given an empty issue_list there is nothing to do.
        self.assertEqual(libjira.update_issue_status([], JIStatus.DEVELOPMENT), [])

        # If none of the issues have a status less than the input, nothing to do.
        self.assertEqual(libjira.update_issue_status([self.JIRA05], JIStatus.DEVELOPMENT),
                         [])

        # Not production, just set the new status and return the list.
        issue1 = deepcopy(self.JIRA01)
        issue2 = deepcopy(self.JIRA02)
        issue3 = deepcopy(self.JIRA03)
        issue4 = deepcopy(self.JIRA04)
        issue_list = [issue1, issue2, issue3, issue4]
        result = libjira.update_issue_status(issue_list, JIStatus.READY_FOR_QA)
        self.assertEqual(result, [issue2, issue3])
        self.assertEqual(issue2.fields.status, 'NEW')
        self.assertEqual(issue3.fields.status, 'DEVELOPMENT')

    @mock.patch('webhook.libjira.is_production', mock.Mock(return_value=True))
    @mock.patch('jira.JIRA.transition_issue')
    @mock.patch('jira.JIRA.add_comment')
    def test_update_issue_status_prod(self, add_comment, transition):
        """Moves qualifying issues to the given status and returns them in a list."""
        # Production, set the new status and return the list.
        issue1 = deepcopy(self.JIRA01)
        issue2 = deepcopy(self.JIRA02)
        issue3 = deepcopy(self.JIRA03)
        issue4 = deepcopy(self.JIRA04)
        issue5 = deepcopy(self.JIRA05)
        issue_list = [issue1, issue2, issue3, issue4, issue5]

        t_comment = "GitLab kernel MR bot updated status to DEVELOPMENT"
        result = libjira.update_issue_status(issue_list, JIStatus.DEVELOPMENT)
        self.assertEqual(transition.call_count, 1)
        transition.assert_called_with(issue2, JIStatus.DEVELOPMENT, comment=t_comment)
        self.assertEqual(result, [issue2])
        self.assertEqual(issue1.fields.status, 'UNKNOWN')
        self.assertEqual(issue2.fields.status, 'DEVELOPMENT')
        self.assertEqual(issue3.fields.status, 'DEVELOPMENT')
        self.assertEqual(issue4.fields.status, 'READY_FOR_QA')
        self.assertEqual(issue5.fields.status, 'TESTED')

        transition.call_count = 0
        with self.assertLogs('cki.webhook.libjira', level='INFO') as logs:
            result = libjira.update_issue_status([issue3], JIStatus.READY_FOR_QA)
            transition.assert_not_called()
            self.assertEqual(result, [])
            self.assertIn("Setting Jira Issue's Preliminary Testing field to Ready",
                          logs.output[-1])

        # test NOT moving issue to TESTED
        with self.assertLogs('cki.webhook.libjira', level='INFO') as logs:
            issue4_status = issue4.fields.status
            result = libjira.update_issue_status([issue4], JIStatus.TESTED)
            self.assertEqual(result, [])
            self.assertIn('Unsupported transition status: TESTED', logs.output[-1])
            self.assertEqual(issue4.fields.status, issue4_status)

    @mock.patch('webhook.libjira.is_production', mock.Mock(return_value=True))
    @mock.patch('webhook.libjira.issues_with_lower_status')
    def test_update_testable_builds(self, mock_ls):
        fields1 = mock.Mock(status=21, customfield_12321740='Contains pipeline_url in it')
        fields2 = mock.Mock(status=21, customfield_12321740='garbage')
        updater1 = mock.Mock()
        updater2 = mock.Mock()
        issue1 = mock.Mock(key=f'{JPFX}11223344', fields=fields1, update=updater1)
        issue2 = mock.Mock(key=f'{JPFX}22334455', fields=fields2, update=updater2)
        issue_list = [issue1, issue2]

        # No issue objects, nothing to return.
        mock_ls.return_value = []
        libjira.update_testable_builds(issue_list, 'text', ['pipeline_url'])
        issue1.update.assert_not_called()
        issue2.update.assert_not_called()

        # Comment already posted to one issue but not the other
        mock_ls.return_value = issue_list
        with self.assertLogs('cki.webhook.libjira', level='DEBUG') as logs:
            libjira.update_testable_builds(issue_list, 'text', ['pipeline_url'])
            self.assertIn(f'All downstream pipelines found in {JPFX}11223344', logs.output[-1])
            issue1.update.assert_not_called()
            issue2.update.assert_called_with(fields={'customfield_12321740': 'text'})

    @mock.patch('webhook.libjira.is_production', mock.Mock(return_value=True))
    @mock.patch('webhook.libjira.connect_jira')
    def test_add_gitlab_link_in_issues(self, mock_jira):
        """Test gitlab link additions in jira issues."""
        mr_id = 666
        url = f'{GITFORGE}/foo/bar/-/merge_requests/{mr_id}'
        icon_url = (f'{GITFORGE}/assets/favicon-'
                    '72a2cad5025aa931d6ea56c3201d1f18e68a8cd39788c7c80d5b2b82aa5143ef.png')
        icon = {'url16x16': icon_url, 'title': 'GitLab Merge Request'}
        title = "A great MR"
        obj = mock.Mock(url=url, title=f'Merge Request: {title}', icon=icon)
        remote_link = mock.Mock(id=1112, object=obj)
        mock_jira().remote_link.return_value = remote_link
        mock_jira().remote_links.return_value = [remote_link]
        mock_MR = mock.Mock(mr_id=mr_id,
                            commits=True,
                            title=title,
                            description=mock.Mock(bugzilla=False),
                            namespace='foo/bar')
        mock_issue = mock.Mock(key=f'{JPFX}1234')
        with self.assertLogs('cki.webhook.libjira', level='INFO') as logs:
            libjira.add_gitlab_link_in_issues([mock_issue], mock_MR)
            self.assertIn(f'MR {mr_id} already linked in {JPFX}1234',
                          logs.output[-1])
            mock_jira().remote_links.return_value = []
            libjira.add_gitlab_link_in_issues([mock_issue], mock_MR)
            self.assertIn(f'Linking [Merge Request: A great MR]({url}) to issue {JPFX}1234',
                          logs.output[-1])
            exp_obj = {'url': url, 'title': f'Merge Request: {title}', 'icon': icon}
            mock_jira().add_simple_link.assert_called_with(issue=mock_issue, object=exp_obj)
            mock_jira().add_comment.assert_called_once()

    def test_remove_gitlab_link_comment_in_issue(self):
        """Test removal of comments we left in the issue pointing to our MR."""
        mock_issue = mock.Mock(id=55, key='RHEL-55')
        mock_author = mock.Mock()
        mock_author.name = 'gitlab-jira'
        mr_url = f'{GITFORGE}/foo/bar/-/merge_requests/66'
        mock_comment = mock.Mock(id=1234, body=f"Contains {mr_url} in it", author=mock_author)
        mock_jira = mock.Mock()
        mock_jira.comments.return_value = [mock_comment]
        mock_jira.comment.return_value = mock_comment
        with self.assertLogs('cki.webhook.libjira', level='INFO') as logs:
            libjira.remove_gitlab_link_comment_in_issue(mock_jira, mock_issue, mr_url)
            self.assertIn(f"Removing {mock_issue.key} comment pointing to {mr_url}",
                          logs.output[-1])
            mock_comment.delete.assert_called_once()

    @mock.patch('webhook.libjira._getissues')
    @mock.patch('webhook.libjira.connect_jira')
    def test_remove_gitlab_link_in_issues(self, mock_jira, mock_getissues):
        """Test gitlab link additions in jira issues."""
        mr_id = 666
        url = f'{GITFORGE}/foo/bar/-/merge_requests/{mr_id}'
        obj = mock.Mock(url=url, title='Merge request - blah blah',
                        icon={'url16x16': 'https://example.com/favicon.png',
                              'title': 'GitLab'})
        remote_link = mock.Mock(id=1112, object=obj)
        mock_jira().remote_link.return_value = remote_link
        mock_jira().remote_links.return_value = [remote_link]
        namespace = 'foo/bar'
        mock_getissues.return_value = [fakes_jira.JI7777777]

        # Ensure we do nothing if there's no issue list
        libjira.remove_gitlab_link_in_issues(mr_id, namespace, set())
        mock_getissues.assert_not_called()

        # make sure we call unlink functions when we do get an issue to remove
        with self.assertLogs('cki.webhook.libjira', level='INFO') as logs:
            libjira.remove_gitlab_link_in_issues(mr_id, namespace, [f'{JPFX}7777777'])
            self.assertIn(f'MR {mr_id} linked in {JPFX}7777777, removing it',
                          logs.output[-1])
