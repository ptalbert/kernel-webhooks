"""Owners tests."""
from unittest import TestCase

from webhook import owners

OWNERS_HEADER = "subsystems:\n"

OWNERS_ENTRY_1 = (" - subsystem: A dummy subsystem\n"
                  "   maintainers: &group0\n"
                  "     - name: User 0\n"
                  "       email: user0@redhat.com\n")

OWNERS_ENTRY_2 = (" - subsystem: Some Subsystem\n"
                  "   labels:\n"
                  "     name: redhat\n"
                  "     readyForMergeDeps:\n"
                  "       - testDep\n"
                  "   status: Maintained\n"
                  "   requiredApproval: true\n"
                  "   maintainers:\n"
                  "     - name: User 1\n"
                  "       email: user1@redhat.com\n"
                  "     - name: User 2\n"
                  "       email: user2@redhat.com\n"
                  "   reviewers:\n"
                  "     - *group0\n"
                  "     - name: User 3\n"
                  "       email: user3@redhat.com\n"
                  "     - name: User 4\n"
                  "       email: user4@redhat.com\n"
                  "       restricted: true\n"
                  "   devel-sst:\n"
                  "     - rhel-sst-something\n"
                  "   qe-sst:\n"
                  "     - rhel-sst-somethingelse\n"
                  "   paths:\n"
                  "       includes:\n"
                  "          - makefile\n"
                  "          - redhat/\n"
                  "       includeRegexes:\n"
                  "          - bpf\n"
                  "       excludes:\n"
                  "          - redhat/configs/\n"
                  "   scm: git https://gitlab.com/cki-project/kernel-ark.git\n"
                  "   mailingList: https://somelist.redhat.com/xxx/\n")


class TestOwners(TestCase):

    def test_empty_parser(self):
        parser = owners.Parser(None)
        self.assertEqual(len(parser.get_all_entries()), 0)

        parser = owners.Parser("")
        self.assertEqual(len(parser.get_all_entries()), 0)

        parser = owners.Parser("subsystems:\n - invalid: Does Not Match\n")
        entries = parser.get_all_entries()
        self.assertEqual(len(entries), 1)

        entry = entries[0]
        self.assertEqual(entry.subsystem_name, '')
        self.assertEqual(entry.subsystem_label, '')
        self.assertEqual(entry.ready_for_merge_label_deps, [])
        self.assertEqual(entry.status, '')
        self.assertEqual(entry.required_approvals, False)
        self.assertEqual(entry.maintainers, [])
        self.assertEqual(entry.reviewers, [])
        self.assertEqual(entry.scm, '')
        self.assertEqual(entry.mailing_list, '')
        self.assertEqual(entry.path_include_regexes, [])
        self.assertEqual(entry.path_includes, [])
        self.assertEqual(entry.path_excludes, [])
        self.assertFalse(entry.matches(['makefile']))

    def test_parser(self):
        parser = owners.Parser(OWNERS_HEADER + OWNERS_ENTRY_1 + OWNERS_ENTRY_2)
        entries = parser.get_all_entries()
        self.assertEqual(len(entries), 2)

        entry = entries[1]
        self.assertEqual(entry.subsystem_name, 'Some Subsystem')
        self.assertEqual(entry.subsystem_label, 'redhat')
        self.assertEqual(entry.ready_for_merge_label_deps, ['testDep'])
        self.assertEqual(entry.status, 'Maintained')
        self.assertTrue(entry.required_approvals)
        self.assertEqual(entry.maintainers, [{'name': 'User 1', 'email': 'user1@redhat.com'},
                                             {'name': 'User 2', 'email': 'user2@redhat.com'}])
        self.assertEqual(entry.reviewers, [{'name': 'User 0', 'email': 'user0@redhat.com'},
                                           {'name': 'User 3', 'email': 'user3@redhat.com'},
                                           {'name': 'User 4', 'email': 'user4@redhat.com',
                                            'restricted': True}])

        self.assertEqual(entry.scm, 'git https://gitlab.com/cki-project/kernel-ark.git')
        self.assertEqual(entry.mailing_list, 'https://somelist.redhat.com/xxx/')
        self.assertEqual(entry.path_include_regexes, ['bpf'])
        self.assertEqual(entry.path_includes, ['makefile', 'redhat/'])
        self.assertEqual(entry.path_excludes, ['redhat/configs/'])
        self.assertEqual(str(entry), 'Some Subsystem')
        self.assertEqual(entry.devel_sst, ['rhel-sst-something'])
        self.assertEqual(entry.qe_sst, ['rhel-sst-somethingelse'])

        self.assertTrue(entry.matches(['makefile']))
        self.assertTrue(entry.matches(['redhat/']))
        self.assertTrue(entry.matches(['redhat/Makefile']))
        self.assertTrue(entry.matches(['include/linux/bpf.h']))
        self.assertFalse(entry.matches(['redhat/configs/generic/CONFIG_NET']))
        self.assertFalse(entry.matches(['drivers/iio/light/tsl2772.c']))
        self.assertTrue(entry.matches(['makefile', 'redhat/configs/generic/CONFIG_NET']))

        self.assertEqual(len(parser.get_matching_entries(['makefile'])), 1)
        self.assertEqual(len(parser.get_matching_entries(['redhat/'])), 1)
        self.assertEqual(len(parser.get_matching_entries(['redhat/Makefile'])), 1)
        self.assertEqual(len(parser.get_matching_entries(['redhat/configs/generic/CONFIG_NET'])), 0)
        self.assertEqual(len(parser.get_matching_entries(['drivers/iio/light/tsl2772.c'])), 0)

        self.assertEqual(parser.get_matching_entries_by_label('redhat'), [parser._entries[1]])

    def test_parser_null_entries(self):
        """Null values for maintainers or reviewers in the yaml should return an empty list."""
        owners_yaml = ("subsystems:\n"
                       " - subsystem: Some Subsystem\n"
                       "   labels:\n"
                       "     readyForMergeDeps:\n"
                       "   maintainers:\n"
                       "   reviewers:\n"
                       "   paths:\n"
                       "       includes:\n"
                       "          - redhat/\n")
        parser = owners.Parser(owners_yaml)
        entry = parser.get_all_entries()[0]
        self.assertEqual(entry.maintainers, [])
        self.assertEqual(entry.reviewers, [])
        self.assertEqual(entry.ready_for_merge_label_deps, [])

    def test_glob_match(self):
        result = owners.glob_match('usr/bin/false', 'usr/*')
        self.assertEqual(result, False)
        result = owners.glob_match('usr/bin/false', 'usr/bin/*')
        self.assertEqual(result, True)
        result = owners.glob_match('usr/bin/false', 'usr/bin/f[ab]*')
        self.assertEqual(result, True)
        result = owners.glob_match('usr/bin/false', 'usr/bin/f[xy]*')
        self.assertEqual(result, False)
        result = owners.glob_match('usr/bin/false', 'usr/bin/*al*')
        self.assertEqual(result, True)
        result = owners.glob_match('usr/bin/false', 'usr/bin/false')
        self.assertEqual(result, True)
        result = owners.glob_match('usr/bin/false', 'usr/*/false')
        self.assertEqual(result, True)
        result = owners.glob_match('usr/bin/false', 'usr/???/false')
        self.assertEqual(result, True)
        result = owners.glob_match('usr/bin/false', 'usr/????/false')
        self.assertEqual(result, False)
        result = owners.glob_match('usr/bin/false', '*')
        self.assertEqual(result, False)
        result = owners.glob_match('usr/bin/false', '*/*')
        self.assertEqual(result, False)
        result = owners.glob_match('usr/bin/false', '*/*/*')
        self.assertEqual(result, True)
        result = owners.glob_match('usr/bin/false', 'usr/')
        self.assertEqual(result, True)
        result = owners.glob_match('usr/bin/false', 'usr/bin/')
        self.assertEqual(result, True)
        result = owners.glob_match('usr/bin/false', 'usr/sbin/')
        self.assertEqual(result, False)
        result = owners.glob_match('usr/bin/false', '*/')
        self.assertEqual(result, True)
        result = owners.glob_match('usr/bin/false', 'usr/bin')
        self.assertEqual(result, False)
